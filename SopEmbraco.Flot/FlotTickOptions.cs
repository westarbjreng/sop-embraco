﻿namespace SopEmbraco.Flot
{
    using System.Collections.Generic;
    using System.Linq;

    public class FlotTickOptions : FlotElementBase
    {
        private List<FlotTick> mTicks;

        public List<FlotTick> Ticks
        {
            get
            {
                if (mTicks == null)
                    mTicks = new List<FlotTick>();
                return mTicks;
            }
        }

        public int? Number { get; set; }

        public void Add(double value)
        {
            Ticks.Add(new FlotTick(value));
        }

        public void Add(double value, string label)
        {
            Ticks.Add(new FlotTick(value, label));
        }

        public void Add(double value, double label)
        {
            Ticks.Add(new FlotTick(value, label));
        }

        public static implicit operator FlotTickOptions(int number)
        {
            return new FlotTickOptions()
            {
                Number = number
            };
        }

        public static implicit operator FlotTickOptions(string function)
        {
            return new FlotTickOptions()
            {
                Function = function
            };
        }

        public static implicit operator FlotTickOptions(FlotTick[] ticks)
        {
            FlotTickOptions tick = new FlotTickOptions();
            tick.Ticks.AddRange(ticks);
            return tick;
        }

        public string Function { get; set; }

        public override string Serialize()
        {
            // can be number, array, or function
            string ticks = string.Empty;

            if (!string.IsNullOrEmpty(Function))
            {
                ticks = Function;
            }
            else if (Ticks.Count > 0)
            {
                ticks = "[" + string.Join(", ", (from t in Ticks select t.Serialize()).ToArray()) + "]";
            }
            else if (Number.HasValue)
            {
                ticks = Number.Value.ToString();
            }

            return ticks;
        }
    }
}