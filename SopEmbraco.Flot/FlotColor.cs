﻿namespace SopEmbraco.Flot
{
    using System.Collections.Generic;
    using System.Linq;

    public class FlotColor : FlotElementBase
    {
        private List<string> mColors;
        private List<FlotColorScaling> mScaling;
        private int? mIndex;

        public FlotColor()
        {
            mColors = new List<string>();
            mScaling = new List<FlotColorScaling>();
        }

        public FlotColor(string s) : this()
        {
             mColors.Add(s);
        }

        //public FlotColor(string s, float opacity) : this()
        //{
        //     mColors.Add(s);
        //     mScaling.Add(new FlotColorScaling(opacity, 0));
        //}

        //public FlotColor(FlotColorScaling s) : this()
        //{
        //    mScaling.Add(s);
        //}

        public FlotColor(int index) : this()
        {
            mIndex = index;
        }

        public static implicit operator FlotColor(string s)
        {
            return new FlotColor(s);
        }

        public static implicit operator FlotColor(int i)
        {
            return new FlotColor(i);
        }

        //public static implicit operator FlotColor(FlotColorScaling s)
        //{
        //    return new FlotColor(s);
        //}

        public static implicit operator FlotColor(string[] s)
        {
            FlotColor c = new FlotColor();
            c.AddRange(s);
            return c;
        }

        /// <summary>
        /// Hex colour.
        /// </summary>
        /// <param name="color"></param>
        public void Add(string color)
        {
            mColors.Add(color);
        }

        public void AddRange(IEnumerable<string> color)
        {
            mColors.AddRange(color);
        }

        public override string Serialize()
        {
            string value = string.Empty;
            if (mIndex.HasValue)
                value = mIndex.ToString();

            if (mColors.Count == 1)
                value = "'" + mColors[0] + "'";

            if (mColors.Count > 1)
                value = "colors: [" + string.Join(", ", (from s in mColors select "'" + s + "'").ToArray()) + "]";

            if (mScaling.Count > 1)
                value = "colors: [" + string.Join(", ", (from s in mScaling select "{" + s.Serialize() + "}").ToArray()) + "]";

            // multi color attributes need to be nested in {}
            if (MultiColor && !string.IsNullOrEmpty(value))
                value = "{ " + value + " }";

            return value;
        }

        public bool MultiColor
        {
            get { return mColors.Count >= 2 || mScaling.Count >= 2; }
        }
    }
}