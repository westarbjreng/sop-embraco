﻿namespace SopEmbraco.Flot
{
    public class FlotColorScaling : FlotElementBase
    {
        public FlotColorScaling(double opacity, double brightness)
        {
            Opacity = opacity;
            Brightness = brightness;
        }

        [FlotProperty]
        public double? Opacity { get; set; }
        [FlotProperty]
        public double? Brightness { get; set; }
    }
}