﻿namespace SopEmbraco.Flot
{
    using System.Reflection;

    public abstract class FlotElementBase : IFlotElement
    {
        public string ElementName { get; set; }

        public FlotElementBase()
        {
            object[] attributes = this.GetType().GetCustomAttributes(typeof(FlotPropertyAttribute), true);
            if (attributes.Length == 1)
            {
                ElementName = ((FlotPropertyAttribute)attributes[0]).Name;
            }
        }

        private FlotAttributeCollection mAttributes;
        /// <summary>
        /// Gets the attributes to be serlialized
        /// </summary>
        public FlotAttributeCollection Attributes
        {
            get
            {
                if (mAttributes == null)
                    mAttributes = new FlotAttributeCollection(this);
                return mAttributes;
            }
        }

        /// <summary>
        /// This method determines how the class is serialised to JSON.
        /// Override this method to implement your own custom serialisation.
        /// By default, we look through all public fields and properties that are marked as [FlotProperty].
        /// </summary>
        /// <returns></returns>
        public virtual string Serialize()
        {
            return Attributes.Serialize(true);
        }
    }
}