﻿namespace SopEmbraco.Flot
{
    public interface IFlotElement
    {
        string Serialize();
    }
}