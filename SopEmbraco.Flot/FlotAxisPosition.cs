﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Defines where the axes are to be placed on the chart
    /// </summary>
    public enum FlotAxisPosition
    {
        /// <summary>
        /// The x axis should be positioned to the bottom
        /// </summary>
        Bottom,
        /// <summary>
        /// The x axis should be positioned to the top
        /// </summary>
        Top,
        /// <summary>
        /// The y axis should be positioned to the left
        /// </summary>
        Left,
        /// <summary>
        /// The y axis should be positioned to the right
        /// </summary>
        Right
    }
}