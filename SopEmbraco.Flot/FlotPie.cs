﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Options for display of pie charts.
    /// See http://people.iola.dk/olau/flot/examples/pie.html
    /// </summary>
    [FlotProperty(Name = "pie")]
    public class FlotPie : FlotElementBase
    {
        internal FlotPie() { }

        /// <summary>
        /// Enable the plugin and draw as a pie.
        /// </summary>
        [FlotProperty]
        public bool Show = true;

        /// <summary>
        ///  Sets the radius of the pie. If value is between 0 and 1 (inclusive) then it will use that as a percentage of the available space (size of the container), otherwise it will use the value as a direct pixel length. If set to 'auto', it will be set to 1 if the legend is enabled and 3/4 if not.
        /// </summary>
        [FlotProperty(SerializationOptions=FlotSerializationOptions.QuoteValue)]
        public string Radius = "auto";

        /// <summary>
        /// Sets the radius of the donut hole. If value is between 0 and 1 (inclusive) then it will use that as a percentage of the radius, otherwise it will use the value as a direct pixel length.
        /// </summary>
        [FlotProperty]
        public double InnerRadius = 0;

        /// <summary>
        /// Factor of pi used for the starting angle (in radians) It can range between 0 and 2 (where 0 and 2 have the same result).
        /// </summary>
        [FlotProperty]
        public double StartAngle = 3.0 / 2.0;

        /// <summary>
        /// Percentage of tilt ranging from 0 and 1, where 1 has no change (fully vertical) and 0 is completely flat (fully horizontal -- in which case nothing actually gets drawn).
        /// </summary>
        [FlotProperty]
        public int Tilt = 1;

        /// <summary>
        /// Positioning of the pie chart within the canvas.
        /// </summary>
        [FlotProperty]
        public class FlotPieOffset : FlotElementBase
        {
            /// <summary>
            /// Pixel distance to move the pie up and down (relative to the center).
            /// </summary>
            [FlotProperty]
            public int Top = 0;

            /// <summary>
            /// Pixel distance to move the pie left and right (relative to the center).
            /// </summary>
            [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
            public string Left = "auto";
        }

        /// <summary>
        /// Positioning of the pie chart within the canvas.
        /// </summary>
        [FlotProperty]
        public FlotPieOffset Offset = new FlotPieOffset();

        /// <summary>
        /// Pie slice borders.
        /// </summary>
        [FlotProperty]
        public class FlotPieStroke : FlotElementBase
        {
            /// <summary>
            /// Color of the border of each slice. Hexadecimal color definitions are prefered (other formats may or may not work).
            /// </summary>
            [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
            public string Color = "#fff";

            /// <summary>
            ///  Pixel width of the border of each slice.
            /// </summary>
            [FlotProperty]
            public int Width = 1;
        }

        /// <summary>
        /// Pie slice borders.
        /// </summary>
        [FlotProperty]
        public FlotPieStroke Stroke = new FlotPieStroke();

        /// <summary>
        /// Pie slice label.
        /// </summary>
        [FlotProperty]
        public class FlotPieLabel : FlotElementBase
        {
            /// <summary>
            ///  Enable/Disable the labels. This can be set to true, false, or 'auto'. When set to 'auto', it will be set to false if the legend is enabled and true if not.
            /// </summary>
            [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
            public string Show = "auto";

            /// <summary>
            /// Sets the radius at which to place the labels. If value is between 0 and 1 (inclusive) then it will use that as a percentage of the available space (size of the container), otherwise it will use the value as a direct pixel length.
            /// </summary>
            [FlotProperty]
            public double Radius = 1;

            /// <summary>
            /// Hides the labels of any pie slice that is smaller than the specified percentage (ranging from 0 to 1) i.e. a value of '0.03' will hide all slices 3% or less of the total.
            /// </summary>
            [FlotProperty]
            public double Threshold = 0;

            /// <summary>
            /// This function specifies how the positioned labels should be formatted, and is applied after the legend's labelFormatter function. The labels can also still be styled using the class "pieLabel" (i.e. ".pieLabel" or "#graph1 .pieLabel").
            /// The value of this property must be valid JavaScript; it is not quoted by the serialisation.
            /// </summary>
            [FlotProperty]
            public string Formatter = "function(label, series){return '<div style=\"font-size:8pt;text-align:center;padding:2px;color:white;\">'+label+'<br/>'+Math.round(series.percent)+'%</div>';}";

            /// <summary>
            /// Pie slice label background.
            /// </summary>
            [FlotProperty]
            public class FlotPieLabelBackground : FlotElementBase
            {
                /// <summary>
                /// Colour of the label background.
                /// </summary>
                [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
                public string Color = "#000";

                /// <summary>
                /// Opacity of the label background
                /// </summary>
                [FlotProperty]
                public double Opacity = 0.5;
            }

            /// <summary>
            /// Background color of the positioned labels.
            /// </summary>
            [FlotProperty]
            public FlotPieLabelBackground Background = new FlotPieLabelBackground();
        }

        /// <summary>
        /// Pie slice label.
        /// </summary>
        [FlotProperty]
        public FlotPieLabel Label = new FlotPieLabel();

        /// <summary>
        /// Combines slices that are smaller than the specified percentage.
        /// </summary>
        [FlotProperty]
        public class FlotPieCombine : FlotElementBase
        {
            /// <summary>
            /// Combines all slices that are smaller than the specified percentage (ranging from 0 to 1) i.e. a value of '0.03' will combine all slices 3% or less into one slice).
            /// </summary>
            [FlotProperty]
            public double Threshold = 0;

            /// <summary>
            /// Backgound color of the positioned labels. If null, the plugin will automatically use the color of the first slice to be combined.
            /// </summary>
            [FlotProperty]
            public FlotColor Color;

            /// <summary>
            /// Label text for the combined slice.
            /// </summary>
            [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
            public string Label = "Other";
        }

        /// <summary>
        /// Combines slices that are smaller than the specified percentage.
        /// </summary>
        [FlotProperty]
        public FlotPieCombine Combine = new FlotPieCombine();

        /// <summary>
        /// This option is not used because any value causes the hover to fill the slice black. The manual says this:
        /// Opacity of the highlight overlay on top of the current pie slice. Currently this just uses a white overlay, but support for changing the color of the overlay will also be added at a later date. 
        /// </summary>
        //[FlotProperty]
        public double Highlight = 0.5;

    }
}