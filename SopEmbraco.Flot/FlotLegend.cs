﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// The legend is generated as a table with the data series labels and small label boxes with the color of the series
    /// </summary>
    [FlotProperty(Name = "legend")]
    public class FlotLegend : FlotElementBase
    {
        /// <summary>
        /// Gets or sets a value which indicates that the legend should be shown
        /// </summary>
        [FlotProperty]
        public bool? Show { get; set; }
        /// <summary>
        /// If you want to format the labels in some way, e.g. make them to links, you can pass in a function for "labelFormatter".
        /// The value can be null or (fn: string, series object -> string)
        /// </summary>
        [FlotProperty]
        public string LabelFormatter { get; set; }
        [FlotProperty]
        public FlotColor LabelBoxBorderColor { get; set; }
        /// <summary>
        /// The number of columns to divide the legend table into
        /// </summary>
        [FlotProperty]
        public int? NoColumns { get; set; }
        /// <summary>
        /// Specifies the overall placement of the legend within the plot (top-right, top-left, etc.) "ne" or "nw" or "se" or "sw"
        /// </summary>
        [FlotProperty]
        public string Position { get; set; }
        /// <summary>
        /// Number of pixels or [x margin, y margin]
        /// </summary>
        [FlotProperty]
        public string Margin { get; set; }
        /// <summary>
        /// null or color
        /// </summary>
        [FlotProperty]
        public FlotColor BackgroundColor { get; set; }
        /// <summary>
        /// number between 0 and 1
        /// </summary>
        [FlotProperty]
        public double? BackgroundOpacity { get; set; }
        /// <summary>
        /// If you want the legend to appear somewhere else in the DOM, you can specify "container" as a jQuery object/expression to put the legend table into.
        /// The "position" and "margin" etc. options will then be ignored.
        /// Note that Flot will overwrite the contents of the container.
        /// The value can be null or jQuery object/DOM element/jQuery expression
        /// </summary>
        [FlotProperty]
        public string Container { get; set; }
    }
}