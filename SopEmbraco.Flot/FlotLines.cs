﻿namespace SopEmbraco.Flot
{
    [FlotProperty(Name = "lines")]
    public class FlotLines : FlotOptionsBase
    {
        internal FlotLines() { }
        /// <summary>
        /// boolean
        /// </summary>
        [FlotProperty]
        public bool? Steps { get; set; }
    }
}