﻿namespace SopEmbraco.Flot
{
    public class FlotTick
    {
        public double Value { get; set; }
        public string Label { get; set; }
        public double? TimeLabel { get; set; }

        public FlotTick(double value)
        {
            Value = value;
        }

        public FlotTick(double value, string label)
        {
            Value = value;
            Label = label;
        }

        public FlotTick(double value, double timeLabel)
        {
            Value = value;
            TimeLabel = timeLabel;
        }

        public static implicit operator FlotTick(double value)
        {
            return new FlotTick(value);
        }

        internal string Serialize()
        {
            string label = string.Empty;

            if (!string.IsNullOrEmpty(Label))
                label = "'" + Label.Replace("'", "\\'") + "'";

            if (TimeLabel.HasValue)
                label = TimeLabel.Value.ToString();

            bool hasLabel = !string.IsNullOrEmpty(label);

            return (hasLabel ? "[" : string.Empty) + Value + (!hasLabel ? string.Empty : ", " + label + (hasLabel ? "]" : string.Empty));
        }
    }
}