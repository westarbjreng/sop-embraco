﻿namespace SopEmbraco.Flot
{
    public class FlotFill : FlotElementBase
    {
        public double? Fill { get; internal set; }
        public bool? FullFill { get; internal set; }

        public static implicit operator FlotFill(double d)
        {
            return new FlotFill()
            {
                Fill = d
            };
        }

        public static implicit operator FlotFill(bool b)
        {
            return new FlotFill()
            {
                FullFill = b
            };
        }

        public override string Serialize()
        {
            if (Fill.HasValue)
                return Fill.ToString();

            if (FullFill.HasValue)
                return FullFill.ToString().ToLower();

            return string.Empty;
        }
    }
}