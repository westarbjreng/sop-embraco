﻿namespace SopEmbraco.Flot
{
    using System;

    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Class, AllowMultiple = false)]
    class FlotPropertyAttribute : Attribute
    {
        /// <summary>
        /// Used only for classes at the root of a serialisation tree. Lower level classes are serialised against the name of the field/property that holds them.
        /// </summary>
        public string Name { get; set; }

        public FlotSerializationOptions SerializationOptions { get; set; }
        
        /// <summary>
        /// ?
        /// </summary>
        public bool NestedProperty { get; set; }

        public FlotPropertyAttribute()
        {
        }
    }
}
