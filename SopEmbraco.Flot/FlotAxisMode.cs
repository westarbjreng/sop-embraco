﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Defines how the values on axes are displayed
    /// </summary>
    public enum FlotAxisMode
    {
        /// <summary>
        /// The axis is displayed as decimal numbers
        /// </summary>
        Decimal,
        /// <summary>
        /// The axis is displayed as time
        /// </summary>
        Time
    }
}