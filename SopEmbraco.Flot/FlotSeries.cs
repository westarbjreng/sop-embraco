﻿namespace SopEmbraco.Flot
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// The data points which are plotted on the chart
    /// </summary>
    public class FlotSeries : FlotElementBase
    {
        private FlotDataCollection mData;
        private FlotBars mBars;
        private FlotPoints mPoints;
        private FlotLines mLines;
        private List<string> mColors;

        /// <summary>
        /// To create the Pie series, call CreateSeriesPie on a chart object.
        /// </summary>
        public FlotPie Pie;

        public FlotSeries() { }

        /// <summary>
        /// Gets the unique identifier for the series
        /// </summary>
        [FlotProperty(Name = "id", SerializationOptions = FlotSerializationOptions.QuoteValue)]
        public string ID { get; internal set; }
        
        /// <summary>
        /// Gets or sets the the series color. This is the line of a line chart the the outline of a bar.
        /// To set the fill of a bar, use Series.Bars.FillColour.
        /// </summary>
        [FlotProperty]
        public FlotColor Color { get; set; }

        /// <summary>
        /// Gets or sets the the series fill color.
        /// Setting a fil colour for a line chart makes it into an area chart.
        /// </summary>
        [FlotProperty]
        public FlotColor FillColor { get; set; }

        /// <summary>
        /// Gets or sets the data which will be plotted on the chart
        /// </summary>
        public FlotDataCollection Data
        {
            get
            {
                if (mData == null)
                    mData = new FlotDataCollection();
                return mData;
            }
            set { mData = value; }
        }
        
        [FlotProperty(Name = "data")]
        public string DataValue
        {
            get
            {
                return "[" + string.Join(", ", Data.Select(d => d.ToString()).ToArray()) + "]";
            }
        }

        /// <summary>
        /// The label is used for the legend, if you don't specify one, the series will not show up in the legend.
        /// </summary>
        [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
        public string Label { get; set; }
        
        /// <summary>
        /// Gets the options for displaying points on the series
        /// </summary>
        public FlotPoints Points
        {
            get
            {
                if (mPoints == null)
                    mPoints = new FlotPoints();
                return mPoints;
            }
        }

        /// <summary>
        /// Gets the options for displaying bars on the series
        /// </summary>
        public FlotBars Bars
        {
            get
            {
                if (mBars == null)
                    mBars = new FlotBars();
                return mBars;
            }
        }

        /// <summary>
        /// Gets the options for displaying lines on the series
        /// </summary>
        public FlotLines Lines
        {
            get
            {
                if (mLines == null)
                    mLines = new FlotLines();
                return mLines;
            }
        }

        /// <summary>
        /// The 1-based index of the X-axis against which this data series is to be plotted.
        /// </summary>
        [FlotProperty]
        public int? XAxis { get; set; }
        
        /// <summary>
        /// The 1-based index of the Y-axis against which this data series is to be plotted.
        /// </summary>
        [FlotProperty]
        public int? YAxis { get; set; }
        
        [FlotProperty]
        public bool? Clickable { get; set; }
        
        [FlotProperty]
        public bool? Hoverable { get; set; }
        
        [FlotProperty]
        public int? ShadowSize { get; set; }
        
        public IList<string> Colors
        {
            get
            {
                // TODO: how are these rendered?
                if (mColors == null)
                    mColors = new List<string>();
                return mColors;
            }
        }

        /// <summary>
        /// Value can be "left" or "center"
        /// </summary>
        [FlotProperty(SerializationOptions = FlotSerializationOptions.LowerCase | FlotSerializationOptions.QuoteValue)]
        public FlotBarAlign? Align { get; set; }

        public override string Serialize()
        {
            Attributes.Add(Points.Serialize());
            Attributes.Add(Bars.Serialize());
            Attributes.Add(Lines.Serialize());
            
            if (Pie != null)
            {
                Attributes.Add(Pie.Serialize());
            }

            return Attributes.Serialize(true) ;
        }
    }
}