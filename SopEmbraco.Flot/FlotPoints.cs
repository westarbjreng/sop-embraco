﻿namespace SopEmbraco.Flot
{
    [FlotProperty(Name = "points")]
    public class FlotPoints : FlotOptionsBase
    {
        internal FlotPoints() { }
        /// <summary>
        /// The radius of the symbol
        /// </summary>
        [FlotProperty]
        public int? Radius { get; set; }
        /// <summary>
        /// Value can be "circle" or function
        /// </summary>
        [FlotProperty(SerializationOptions = FlotSerializationOptions.QuoteValue)]
        public string Symbol { get; set; }
    }
}