﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Defines the align options for flot elements
    /// </summary>
    public enum FlotBarAlign
    {
        /// <summary>
        /// Left aligned
        /// </summary>
        Left,
        /// <summary>
        /// Right aligned
        /// </summary>
        Right,
        /// <summary>
        /// Center aligned
        /// </summary>
        Center
    }
}
