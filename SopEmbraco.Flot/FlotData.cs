﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Represents an (x,y)-pair to be plotted on a chart, or a (lable, value) pair to be plotted on a pie chart.
    /// </summary>
    public class FlotData
    {
        /// <summary>
        /// Gets or sets the value for the x-axis
        /// </summary>
        public object X { get; set; }

        /// <summary>
        /// Gets or sets the value for the y-axis
        /// </summary>
        public double? Y { get; set; }

        /// <summary>
        /// Creates a new data plot with no axis values
        /// </summary>
        public FlotData() { }

        /// <summary>
        /// Creates a new data plot with specific values.
        /// Use for bar, line, and scatter plots.
        /// </summary>
        /// <param name="x">The value for the x-axis</param>
        /// <param name="y">The value for the y-axis</param>
        public FlotData(double? x, double? y)
        {
            X = x;
            Y = y;
        }

        /// <summary>
        /// Data point with no X value but with a label: this is for Pie charts.
        /// </summary>
        /// <param name="label"></param>
        /// <param name="value"></param>
        public FlotData(string label, double value)
        {
            Label = label;
            Y = value;
        }

        /// <summary>
        /// Creates a new data plot with specific values.
        /// Use for bar, line, and scatter plots.
        /// </summary>
        /// <param name="x">The value for the x-axis</param>
        /// <param name="y">The value for the y-axis</param>
        /// <param name="label"></param>
        public FlotData(double x, double y, string label)
        {
            X = x;
            Y = y;
            Label = label;
        }

        /// <summary>
        /// Label (e.g., for Pie charts).
        /// </summary>
        public string Label;


        public override string ToString()
        {
            if (X == null && Y.HasValue && Label != null)
            {
                // Pie.
                // Braces are escaped by doubling them (http://msdn.microsoft.com/en-us/library/txafckwd.aspx).
                return string.Format("{{label:'{0}', data: {1}}}", Label.Replace("'", "\\'"), Y);
            }

            // Default
            return string.Format(
                "[{0}, {1}]",
                X != null ? "\"" + X.ToString() + "\"" : "null",
                Y.HasValue ? Y.Value.ToString("R") : "null"
            );
        }
    }
}