﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// The grid is the thing with the axes and a number of ticks.
    /// </summary>
    [FlotProperty(Name = "grid")]
    public class FlotGrid : FlotElementBase
    {
        [FlotProperty]
        public bool? Show { get; set; }
        [FlotProperty]
        public bool? AboveData { get; set; }
        [FlotProperty]
        public FlotColor Color
        { 
            get; 
            set; }
        /// <summary>
        /// color/gradient or null
        /// </summary>
        [FlotProperty]
        public FlotColor BackgroundColor { get; set; }
        [FlotProperty]
        public int? LabelMargin { get; set; }
        [FlotProperty]
        public int? AxisMargin { get; set; }
        /// <summary>
        /// Array of markings or (fn: axes -> array of markings)
        /// </summary>
        [FlotProperty]
        public string Markings { get; set; }
        [FlotProperty]
        public int? BorderWidth { get; set; }
        /// <summary>
        /// color or null
        /// </summary>
        [FlotProperty]
        public FlotColor BorderColor { get; set; }
        /// <summary>
        /// number or null
        /// </summary>
        [FlotProperty]
        public int? MinBorderMargin { get; set; }
        [FlotProperty]
        public bool? Clickable { get; set; }
        [FlotProperty]
        public bool? Hoverable { get; set; }
        public bool ShowToolTip { get; set; }
        public string ToolTipContent { get; set; }
        [FlotProperty]
        public bool? AutoHighlight { get; set; }
        [FlotProperty]
        public int? MouseActiveRadius { get; set; }
    }
}