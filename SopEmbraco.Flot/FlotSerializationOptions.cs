﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Defines the options for serializing a flot attribute
    /// </summary>
    public enum FlotSerializationOptions
    {
        /// <summary>
        /// No specific options
        /// </summary>
        None = 0,
        /// <summary>
        /// Allow an empty attribute value
        /// </summary>
        AllowEmptyValue = 1,
        /// <summary>
        /// Quote the attribute value
        /// </summary>
        QuoteValue = 2,
        /// <summary>
        /// Make the attribute value lower case
        /// </summary>
        LowerCase = 4
    }
}