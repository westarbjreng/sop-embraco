﻿namespace SopEmbraco.Flot
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// A collection of FlotData objects
    /// </summary>
    public class FlotDataCollection : List<FlotData>
    {
        public FlotDataCollection() { }
        /// <summary>
        /// Adds data to the collection
        /// </summary>
        /// <param name="x">The value of the x-axis</param>
        /// <param name="y">The value of the y-axis</param>
        public void Add(double? x, double? y)
        {
            Add(new FlotData(x, y));
        }

        /// <summary>
        /// Adds (pie) data to the collection.
        /// </summary>
        /// <param name="label">Label for the data point.</param>
        /// <param name="value">Value of the data point.</param>
        public void Add(string label, double value)
        {
            Add(new FlotData(label, value));
        }

        /// <summary>
        /// Populates a collection from an array of data
        /// </summary>
        /// <param name="data">The array of data</param>
        /// <returns>A collection populated with data</returns>
        public static implicit operator FlotDataCollection(FlotData[] data)
        {
            FlotDataCollection col = new FlotDataCollection();
            col.AddRange(data);
            return col;
        }
        /// <summary>
        /// Populates a collection from an array of data.  The x and y values are taken from each pair of array values
        /// </summary>
        /// <param name="data">The array of data</param>
        /// <returns>A collection populated with data</returns>
        public static implicit operator FlotDataCollection(int?[] data)
        {
            FlotDataCollection col = new FlotDataCollection();

            for (int i = 0; i < data.Length; i++)
            {
                col.Add(new FlotData(data[i], data[++i]));
            }
            return col;
        }
        /// <summary>
        /// Populates a collection from an array of data.  The x and y values are taken from each pair of array values
        /// </summary>
        /// <param name="data">The array of data</param>
        /// <returns>A collection populated with data</returns>
        public static implicit operator FlotDataCollection(double?[] data)
        {
            FlotDataCollection col = new FlotDataCollection();

            for (int i = 0; i < data.Length; i++)
            {
                col.Add(new FlotData(data[i], data[++i]));
            }
            return col;
        }
    }
}