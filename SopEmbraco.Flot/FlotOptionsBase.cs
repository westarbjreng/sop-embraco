﻿namespace SopEmbraco.Flot
{
    /// <summary>
    /// Defines the base options available to flot option elements
    /// </summary>
    public abstract class FlotOptionsBase : FlotElementBase
    {
        /// <summary>
        /// Gets or sets a flag which indicates that the element should be shown
        /// </summary>
        [FlotProperty]
        public bool? Show { get; set; }
        /// <summary>
        /// The thickness of the line or outline in pixels. You can set it to 0 to prevent a line or outline from being drawn; this will also hide the shadow.
        /// </summary>
        [FlotProperty]
        public int? LineWidth { get; set; }
        /// <summary>
        /// Whether the shape should be filled.  
        /// For lines, this produces area graphs.
        /// You can adjust the opacity of the fill by setting fill to a number between 0 (fully transparent) and 1 (fully opaque).
        /// </summary>
        [FlotProperty]
        public FlotFill Fill { get; set; }
        /// <summary>
        /// Value can be null or color/gradient
        /// </summary>
        [FlotProperty]
        public FlotColor FillColor { get; set; }
    }
}