﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SopEmbraco.Models;

namespace SopEmbraco.Controllers
{
    public class MachineStopReasonsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MachineStopReasons
        public async Task<ActionResult> Index()
        {
            return View(await db.MachineStopReasons.OrderBy(m=> m.MachineStopMode.Name).ThenBy(m=> m.Name).ToListAsync());
        }

        // GET: MachineStopReasons/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopReason machineStopReason = await db.MachineStopReasons.FindAsync(id);
            if (machineStopReason == null)
            {
                return HttpNotFound();
            }
            return View(machineStopReason);
        }

        // GET: MachineStopReasons/Create
        public ActionResult Create()
        {
            ViewBag.MachineStopModeId = new SelectList(db.MachineStopModes.OrderBy(m=> m.Name), "MachineStopModeId", "Name");
            return View();
        }

        // POST: MachineStopReasons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MachineStopModeId,MachineStopReasonId,Name")] MachineStopReason machineStopReason)
        {
            if (ModelState.IsValid)
            {
                machineStopReason.MachineStopReasonId = Guid.NewGuid();
                db.MachineStopReasons.Add(machineStopReason);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MachineStopModeId = new SelectList(db.MachineStopModes, "MachineStopModeId", "Name", machineStopReason.MachineStopModeId);
            return View(machineStopReason);
        }

        // GET: MachineStopReasons/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopReason machineStopReason = await db.MachineStopReasons.FindAsync(id);
            if (machineStopReason == null)
            {
                return HttpNotFound();
            }

            ViewBag.MachineStopModeId = new SelectList(db.MachineStopModes, "MachineStopModeId", "Name", machineStopReason.MachineStopModeId);
            return View(machineStopReason);
        }

        // POST: MachineStopReasons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MachineStopModeId,MachineStopReasonId,Name")] MachineStopReason machineStopReason)
        {
            if (ModelState.IsValid)
            {
                db.Entry(machineStopReason).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MachineStopModeId = new SelectList(db.MachineStopModes, "MachineStopModeId", "Name", machineStopReason.MachineStopModeId);
            return View(machineStopReason);
        }

        // GET: MachineStopReasons/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopReason machineStopReason = await db.MachineStopReasons.FindAsync(id);
            if (machineStopReason == null)
            {
                return HttpNotFound();
            }
            return View(machineStopReason);
        }

        // POST: MachineStopReasons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            MachineStopReason machineStopReason = await db.MachineStopReasons.FindAsync(id);
            db.MachineStopReasons.Remove(machineStopReason);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<JsonResult> GetAllJson()
        {
            return Json(await db.MachineStopReasons.ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
