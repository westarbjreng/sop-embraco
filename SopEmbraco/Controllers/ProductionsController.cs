﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using SopEmbraco.Models;
using SopEmbraco.ViewModels;
using SopEmbraco.Infrastructure.Helpers;
using OfficeOpenXml;

namespace SopEmbraco.Controllers
{
    public class ProductionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult Index()
        {
            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLines = db.WorkshopLines.ToList(); // new SelectList(db.WorkshopLines, "WorkshopLineId", "Name");
            ViewBag.ProductModels = db.ProductModels.ToList(); // new SelectList(db.ProductModels.OrderBy(m => m.Name), "ProductModelId", "Name");
            ViewBag.ProductCodes = db.ProductCodes.ToList(); // new SelectList(db.ProductCodes.OrderBy(pc => pc.Name), "ProductCodeId", "Name");
            ViewBag.ProductionBatches = db.ProductionBatches.ToList();
            return View();
        }

        public ActionResult Report()
        {
            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLines = db.WorkshopLines.ToList();
            ViewBag.ProductModels = db.ProductModels.ToList();
            ViewBag.ProductCodes = db.ProductCodes.ToList();
            ViewBag.ProductionBatches = db.ProductionBatches.ToList();
            return View();
        }

        [HttpGet]
        public async Task<JsonResult> IndexJson(ProductionViewModel viewModel)
        {
            var query = db.Productions.AsQueryable();

            if (viewModel.WorkshopLineId != null)
            {
                query = query.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
            }

            if (viewModel.ProductionDate != null)
            {
                query = query.Where(p => p.Date == viewModel.ProductionDate);
            }

            if (viewModel.ProductModelId != null)
            {
                query = query.Where(p => p.ProductionBatch.ProductCode.ProductModelId == viewModel.ProductModelId);
            }
            if (viewModel.ProductCodeId != null)
            {
                query = query.Where(p => p.ProductionBatch.ProductCodeId == viewModel.ProductCodeId);
            }
            if (viewModel.ProductionBatchId != null)
            {
                query = query.Where(p => p.ProductionBatchId == viewModel.ProductionBatchId);
            } 

           // query = query.Where(p => p.ProductionBatch.Status == Enums.ProductionBatchStatus.Open);

            return Json(await query.Select(p => new
            {
                Comment = p.Comment,
                Date = p.Date,
                EndTime = p.EndTime,
                ProductionBatchId = p.ProductionBatchId,
                ProductionId = p.ProductionId,
                RealQuantity = p.RealQuantity,
                StartTime = p.StartTime,
                Target = p.Target,
                WorkshopLineId = p.WorkshopLineId,
                PlanQty = p.ProductionBatch.Quantity.ToString(),
                ProductCodeName = p.ProductionBatch.ProductCode.Name.ToString()
            }).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> ReportJson(ProductionReportViewModel viewModel)
        {
            var query = BuildReportQuery(viewModel);

            return Json(await query.Select(p => new
            {
                Comment = p.Comment,
                Date = p.Date,
                EndTime = p.EndTime,
                ProductionBatchId = p.ProductionBatchId,
                ProductionId = p.ProductionId,
                RealQuantity = p.RealQuantity,
                StartTime = p.StartTime,
                Target = p.Target,
                WorkshopLineId = p.WorkshopLineId,
                PlanQty = p.ProductionBatch.Quantity.ToString(),
                ProductCodeName = p.ProductionBatch.ProductCode.Name.ToString()
            }).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> CreateJson([Bind(Include = "ProductionBatchId,PlanQty,ProductCode,Date,StartTime,EndTime,WorkshopLineId,Target,RealQuantity,Comment")] Production production)
        {
            if (ModelState.IsValid)
            {
                production.ProductionId = Guid.NewGuid();
                db.Productions.Add(production);
                await db.SaveChangesAsync();
                return Json(production, JsonRequestBehavior.DenyGet);
            }

            /* foreach (ModelState modelState in ViewData.ModelState.Values)
            {
                foreach (ModelError error in modelState.Errors)
                {
                    DoSomethingWith(error);
                }
            } */

            return Json(new { Status = "Failure" }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public async Task<JsonResult> EditJson([Bind(Include = "ProductionId,ProductionBatchId,PlanQty,ProductCode,Date,StartTime,EndTime,WorkshopLineId,Target,RealQuantity,Comment")] Production production)
        {
            if (ModelState.IsValid)
            {
                db.Entry(production).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Json(production, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Failure" }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteJson([Bind(Include = "ProductionId")] Production production)
        {
            Production productionDb = await db.Productions.FirstOrDefaultAsync(p => p.ProductionId == production.ProductionId);
            db.Productions.Remove(productionDb);
            await db.SaveChangesAsync();
            return Json(new { Status = "Ok" }, JsonRequestBehavior.DenyGet);
        }

        // GET: Productions/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            // Production production = await db.Productions.Include(p => p.WorkshopLine.Workshop).Include(p => p.WorkshopLine.Productions).FindAsync(id);
            WorkshopLine workshopLine = await db.WorkshopLines.Include(wl => wl.Productions).FirstOrDefaultAsync(wl => wl.WorkshopLineId == id);
            if (workshopLine == null)
            {
                return HttpNotFound();
            }
            return View(workshopLine);
        }

        // GET: Productions/Create
        public ActionResult Create()
        {
            ViewBag.ProductionBatches = db.ProductionBatches.Where(pb => pb.Status != Enums.ProductionBatchStatus.Concluded).ToList();

            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLineId = new SelectList(db.WorkshopLines, "WorkshopLineId", "Name");
            var productions = new List<Production>();

            foreach (var timeSuggestion in db.TimeSuggestions.OrderBy(t => t.StartTime))
            {
                productions.Add(new Production {
                    StartTime = timeSuggestion.StartTime,
                    EndTime = timeSuggestion.EndTime
                });
            }
            return View(new ProductionViewModel
            {
                PendingProductions = productions
            });
        }

        // POST: Productions/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(ProductionViewModel viewModel, FormCollection formCollection)
        {
            ViewBag.ProductionBatches = db.ProductionBatches.Where(pb => pb.Status != Enums.ProductionBatchStatus.Concluded).ToList();
            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLineId = new SelectList(db.WorkshopLines, "WorkshopLineId", "Name", viewModel.WorkshopLineId);

            if (ModelState.IsValid)
            {
                // Collision detection
                Production previousProduction = null;
                bool collisions = false;

                // TODO: Convert FormCollection items to typed classes to perform a decent validation. 

                foreach (var productionGroup in viewModel.PendingProductions.OrderBy(p => p.StartTime).GroupBy(p => new { p.Date, p.ProductionBatchId }))
                {
                    foreach (var production in productionGroup)
                    {
                        if (previousProduction == null)
                        {
                            previousProduction = production;
                            continue;
                        }

                        if (production.StartTime < previousProduction.EndTime)
                        {
                            ModelState.AddModelError("", "Time Collision Detected: End Time: " + previousProduction.EndTime + "; Start Time: " + production.StartTime + ";");
                            collisions = true;
                        }

                        previousProduction = production;
                    }

                    previousProduction = null;
                }

                if (collisions) return View(viewModel);

                foreach (var production in viewModel.PendingProductions)
                {
                    production.WorkshopLineId = viewModel.WorkshopLineId;
                    production.ProductionId = Guid.NewGuid();
                    db.Productions.Add(production);
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(viewModel);
        }

        // GET: Productions/Edit/5
        public async Task<ActionResult> Edit(Guid? id, DateTime? ReferenceDate)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var referenceDate = ReferenceDate ?? DateTime.Today;

            var workshopLine = await db.WorkshopLines.Include(wl => wl.Productions).FirstOrDefaultAsync(wl => wl.WorkshopLineId == id);
            if (workshopLine == null)
            {
                return HttpNotFound();
            }

            ViewBag.ProductionBatches = db.ProductionBatches.Where(
                    pb => pb.ProductCode.WorkshopId == workshopLine.WorkshopId && pb.Status != Enums.ProductionBatchStatus.Concluded).ToList();
            ViewBag.WorkshopLineId = new SelectList(db.WorkshopLines, "WorkshopLineId", "Name");

            foreach (var production in workshopLine.Productions)
            {
                production.ProductCodeName = production.ProductionBatch.ProductCode.Name;
                production.PlanQuantity = production.ProductionBatch.Quantity.ToString();
            }

            var timeSuggestions = db.TimeSuggestions.OrderBy(t => t.StartTime).ToList();
            // var concludedProductions = new List<Production>();
            var pendingProductions = new List<Production>();
            // foreach (var timeSuggestion in db.TimeSuggestions.OrderBy(t => t.StartTime))
            foreach (var existingProduction in workshopLine.Productions.Where(p => p.Date == referenceDate).OrderBy(p => p.StartTime).ToList())
            {
                var attemptedProduction = timeSuggestions.FirstOrDefault(ts => ts.StartTime == existingProduction.StartTime && ts.EndTime == existingProduction.EndTime);
                if (attemptedProduction != null)
                {
                    timeSuggestions.Remove(attemptedProduction);
                }

                if (existingProduction.ProductionBatch.Status == Enums.ProductionBatchStatus.Concluded)
                {
                    existingProduction.ProductionBatchStatusConcluded = true;
                    existingProduction.ConcludedBatchName = existingProduction.ProductionBatch.BatchNumber;
                }

                pendingProductions.Add(existingProduction);
            }

            foreach (var timeSuggestion in timeSuggestions)
            {
                pendingProductions.Add(new Production() { WorkshopLineId = workshopLine.WorkshopLineId, StartTime = timeSuggestion.StartTime, EndTime = timeSuggestion.EndTime, Date = referenceDate });
            }

            return View(new ProductionViewModel
            {
                WorkshopLine = workshopLine,
                WorkshopLineId = workshopLine.WorkshopLineId,
                // ConcludedProductions = concludedProductions,
                PendingProductions = pendingProductions
            });
        }

        // POST: Productions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(ProductionViewModel viewModel, FormCollection collection)
        {
            var workshopLine = await db.WorkshopLines.FirstOrDefaultAsync(wl => wl.WorkshopLineId == viewModel.WorkshopLineId);
            ViewBag.ProductionBatches = db.ProductionBatches.Where(
                    pb => pb.ProductCode.WorkshopId == workshopLine.WorkshopId && pb.Status != Enums.ProductionBatchStatus.Concluded).ToList();
            ViewBag.WorkshopLineId = new SelectList(db.WorkshopLines, "WorkshopLineId", "Name", viewModel.WorkshopLineId);

            if (ModelState.IsValid)
            {
                if (viewModel.PendingProductions != null)
                {
                    // Collision detection
                    Production previousProduction = null;
                    bool collisions = false;

                    // TODO: Convert FormCollection items to typed classes to perform a decent validation. 

                    foreach (var productionGroup in viewModel.PendingProductions.OrderBy(p => p.StartTime).GroupBy(p => new { p.Date, p.ProductionBatchId }))
                    {
                        foreach (var production in productionGroup)
                        {
                            if (previousProduction == null)
                            {
                                previousProduction = production;
                                continue;
                            }

                            if (production.StartTime < previousProduction.EndTime)
                            {
                                ModelState.AddModelError("", "Time Collision Detected: End Time: " + previousProduction.EndTime + "; Start Time: " + production.StartTime + ";");
                                collisions = true;
                            }

                            previousProduction = production;
                        }

                        previousProduction = null;
                    }

                    if (collisions) return View(viewModel);
                }

                // Original Productions
                var originalProductions = db.Productions.AsNoTracking().Where(p => p.WorkshopLineId == viewModel.WorkshopLineId).ToList();

                // Deleted Productions
                foreach (var originalProduction in originalProductions)
                {
                    if (viewModel.PendingProductions == null || !viewModel.PendingProductions.Any(p => p.ProductionId == originalProduction.ProductionId))
                    {
                        var deletedProduction = db.Productions.Single(p => p.ProductionId == originalProduction.ProductionId);
                        db.Productions.Remove(deletedProduction);
                        await db.SaveChangesAsync();
                    }
                }

                // Inserted/Modified Productions
                foreach (var production in viewModel.PendingProductions ?? new List<Production>())
                {
                    if (!originalProductions.Any(p => p.ProductionId == production.ProductionId))
                    {
                        // New Production.
                        production.WorkshopLineId = viewModel.WorkshopLineId;
                        production.ProductionId = Guid.NewGuid();
                        db.Productions.Add(production);
                    }
                    else
                    {
                        // Modified Production.
                        db.Entry(production).State = EntityState.Modified;
                    }

                    await db.SaveChangesAsync();
                }

                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            viewModel.WorkshopLine = workshopLine;
            return View(viewModel);
        }

        // GET: Productions/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Production production = await db.Productions.FindAsync(id);
            if (production == null)
            {
                return HttpNotFound();
            }
            return View(production);
        }

        // POST: Productions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Production production = await db.Productions.FindAsync(id);
            db.Productions.Remove(production);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public ActionResult NewProductionRow(Guid? id)
        {
            ViewBag.ProductionBatches = db.ProductionBatches.Where(pb => pb.Status != Enums.ProductionBatchStatus.Concluded).ToList();
            return PartialView("_ProductionRow", new Production { WorkshopLineId = id ?? Guid.Empty });
        }

        public ActionResult ExportToExcel(ProductionReportViewModel viewModel)
        {
            var query = BuildReportQuery(viewModel);

            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "SOP Embraco";
                excelPackage.Workbook.Properties.Title = "Productions and Machine Stops Report";
                var sheet = excelPackage.Workbook.Worksheets.Add("Productions");
                sheet.Name = "Productions";

                ExcelHelper.WriteWorksheet(query.ToList()
                                    .Select(x => new
                                    {
                                        Workshop = x.WorkshopLine.Workshop.Name,
                                        WorkshopLine = x.WorkshopLine.Name,
                                        ProductionBatch = x.ProductionBatch.BatchNumber,
                                        ProductCode = x.ProductionBatch.ProductCode.Name,
                                        Date = x.Date,
                                        StartTime = x.StartTime,
                                        EndTime = x.EndTime,
                                        PlanQuantity = x.PlanQuantity,
                                        Target = x.Target,
                                        RealQuantity = x.RealQuantity,
                                        x.Comment
                                    }).ToList(), sheet);

                var sheet2 = excelPackage.Workbook.Worksheets.Add("Machine Stops");
                sheet2.Name = "Machine Stops";

                var machineStopsQuery = db.MachineStops.AsQueryable();

                if (viewModel.WorkshopLineId != null)
                {
                    machineStopsQuery = machineStopsQuery.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
                }

                if (viewModel.ProductionStartDate > DateTime.MinValue && viewModel.ProductionEndDate != DateTime.MinValue)
                {
                    machineStopsQuery = machineStopsQuery.Where(p => p.Date >= viewModel.ProductionStartDate && p.Date <= viewModel.ProductionEndDate);
                }

                ExcelHelper.WriteWorksheet(machineStopsQuery.ToList()
                                    .Select(x => new
                                    {
                                        Workshop = x.WorkshopLine.Workshop.Name,
                                        WorkshopLine = x.WorkshopLine.Name,
                                        Date = x.Date,
                                        Start = x.Start,
                                        Finish = x.Finish,
                                        x.Comment
                                    }).ToList(), sheet2);

                return File(excelPackage.GetAsByteArray(), System.Net.Mime.MediaTypeNames.Application.Octet, "Productions" + DateTime.Now.ToString() + ".xlsx");
            }                        
        }

        private IQueryable<Production> BuildReportQuery(ProductionReportViewModel viewModel)
        {
            var query = db.Productions.AsQueryable();

            if (viewModel.WorkshopLineId != null)
            {
                query = query.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
            }

            if (viewModel.ProductionStartDate > DateTime.MinValue && viewModel.ProductionEndDate > DateTime.MinValue)
            {
                query = query.Where(p => p.Date >= viewModel.ProductionStartDate && p.Date <= viewModel.ProductionEndDate);
            }

            if (viewModel.ProductModelId != null)
            {
                query = query.Where(p => p.ProductionBatch.ProductCode.ProductModelId == viewModel.ProductModelId);
            }
            if (viewModel.ProductCodeId != null)
            {
                query = query.Where(p => p.ProductionBatch.ProductCodeId == viewModel.ProductCodeId);
            }
            if (viewModel.ProductionBatchId != null)
            {
                query = query.Where(p => p.ProductionBatchId == viewModel.ProductionBatchId);
            }

            return query;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
