﻿using SopEmbraco.Models;
using SopEmbraco.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Diagnostics;
using System.Configuration;
using SopEmbraco.Infrastructure;

namespace SopEmbraco.Controllers
{
    public class DigitalSopController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string directoryFiles = ConfigurationManager.AppSettings["DirectoryFiles"];
        private string domain = ConfigurationManager.AppSettings["Domain"];
        private string username = ConfigurationManager.AppSettings["Username"];
        private string password = ConfigurationManager.AppSettings["Password"];

        // GET: DigitalSop
        public ActionResult Index()
        {
            
            ViewBag.Workshops = db.Workshops.OrderBy(ws => ws.Name);
            ViewBag.ProductCodes = new List<ProductCode>().OrderBy(pc=> pc.Workshop.Name).ThenBy(pc=> pc.Name);
            ViewBag.ProductModels = db.ProductModels.ToList().OrderBy(pm=> pm.Name);
            ViewBag.WorkshopLines = new List<WorkshopLine>().OrderBy(wl=> wl.Workshop.Name).ThenBy(wl=> wl.Name);
            ViewBag.Positions = new List<SelectListItem>();
            return View();
        }

        public ActionResult GetFiles()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetFiles(DigitalSopViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var workshopLine = db.WorkshopLines.FirstOrDefault(w => w.WorkshopLineId == viewModel.WorkshopLineId);
                var productCode = db.ProductCodes.Include(pc => pc.ProductModel).FirstOrDefault(p => p.ProductCodeId == viewModel.ProductCodeId);
                var path = directoryFiles + "/" + workshopLine.Workshop.Name + "/" + productCode.ProductModel.Name + "/" + productCode.Name + "/" + workshopLine.Name;

                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                context.Enter();
                var d = new DirectoryInfo(path);
                var files = d.GetFiles("*.*");

                if (viewModel.Position != null)
                {
                    var file = files.FirstOrDefault(f => f.Name == viewModel.Position.ToString() + ".pdf");
                    if (file != null)
                    {
                        viewModel.Files.Add(new FileViewModel
                        {
                            Name = file.Name,
                            ProductCodeId = viewModel.ProductCodeId,
                            WorkshopLineId = viewModel.WorkshopLineId
                        });
                    }
                    else
                    {
                        file = files.FirstOrDefault(f => f.Name == viewModel.Position.ToString() + ".jpg");
                        if (file != null)
                        {
                            viewModel.Files.Add(new FileViewModel
                            {
                                Name = file.Name,
                                ProductCodeId = viewModel.ProductCodeId,
                                WorkshopLineId = viewModel.WorkshopLineId
                            });
                        }
                    }
                }
                else
                {
                    foreach (FileInfo file in files)
                    {
                        viewModel.Files.Add(new FileViewModel
                        {
                            Name = file.Name,
                            ProductCodeId = viewModel.ProductCodeId,
                            WorkshopLineId = viewModel.WorkshopLineId
                        });
                    }
                }
                context.Leave();
            }

            ViewBag.Workshops = db.Workshops;
            if (viewModel.WorkshopId != null)
            {
                ViewBag.ProductCodes = db.ProductCodes.Where(pc => pc.WorkshopId == viewModel.WorkshopId);
                ViewBag.WorkshopLines = db.WorkshopLines.Where(wl => wl.WorkshopId == viewModel.WorkshopId);

                if (viewModel.WorkshopLineId == null) ViewBag.Positions = new List<SelectListItem>();
                var workshopLine = db.WorkshopLines.FirstOrDefault(w => w.WorkshopLineId == viewModel.WorkshopLineId);
                if (workshopLine == null) ViewBag.Positions = new List<SelectListItem>();

                ViewBag.Positions = Enumerable.Range(1, workshopLine.Positions)
                    .Select(p => new SelectListItem
                    {
                        Text = p.ToString(),
                        Value = p.ToString(),
                        Selected = p == viewModel.Position
                    })
                    .ToList();
            }
            else
            {
                ViewBag.ProductCodes = new List<ProductCode>();
                ViewBag.WorkshopLines = new List<WorkshopLine>();
                ViewBag.Positions = new List<SelectListItem>();
            }

            ViewBag.ProductModels = db.ProductModels.ToList();
            return View("Index", viewModel);
        }

        public ActionResult Download(Guid ProductCodeId, Guid WorkshopLineId, String fileId)
        {

            var workshopLine = db.WorkshopLines.FirstOrDefault(w => w.WorkshopLineId == WorkshopLineId);
            var productCode = db.ProductCodes.FirstOrDefault(p => p.ProductCodeId == ProductCodeId);

            WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
            context.Enter();
            byte[] file = System.IO.File.ReadAllBytes(
                Path.Combine(directoryFiles + workshopLine.Workshop.Name + "/" + productCode.ProductModel.Name + "/" + productCode.Name + "/" + workshopLine.Name,
                fileId));

            return File(file, System.Net.Mime.MediaTypeNames.Application.Octet, fileId);
        }

    }
}