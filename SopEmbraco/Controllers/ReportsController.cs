﻿using SopEmbraco.Models;
using SopEmbraco.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Web.Mvc;
using SopEmbraco.Infrastructure.Extensions;

namespace SopEmbraco.Controllers
{
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Reports
        [HttpGet]
        public ActionResult Index(ReportsViewModel viewModel)
        {
            if (viewModel != null)
            {
                if (viewModel.WorkshopId != null)
                {
                    var workshopLines = db.WorkshopLines.Where(wl => wl.WorkshopId == viewModel.WorkshopId).ToList();
                    if (viewModel.Date == null || viewModel.Date == DateTime.MinValue) viewModel.Date = DateTime.Today;
                    viewModel.WorkshopLineReports = new List<ReportsWorkshopLineViewModel>();
                    foreach (var workshopLine in workshopLines)
                    {
                        var newReportEntry = new ReportsWorkshopLineViewModel();
                        newReportEntry.WorkshopLine = workshopLine;
                        newReportEntry.RealQuantities = new Flot.FlotDataCollection { };
                        newReportEntry.Targets = new Flot.FlotDataCollection { };
                        ProductionBatch lastProductionBatch = null;

                        foreach (var production in workshopLine.Productions.Where(p => p.Date == viewModel.Date).OrderBy(p => p.Date).ThenBy(p => p.StartTime))
                        {
                            // var absDate = production.Date.Add(production.StartTime).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                            var absDate = production.Date.Add(production.StartTime);
                            var absDateEnd = production.Date.Add(production.EndTime);
                            newReportEntry.RealQuantities.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("HH:mm") + "-" + absDateEnd.ToString("HH:mm"),
                                Y = production.RealQuantity
                            });
                            newReportEntry.Targets.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("HH:mm") + "-" + absDateEnd.ToString("HH:mm"),
                                Y = production.Target
                            });
                            lastProductionBatch = production.ProductionBatch;
                        }

                        newReportEntry.ProductionBatch = lastProductionBatch;
                        viewModel.WorkshopLineReports.Add(newReportEntry);
                    }
                }
            }

            ViewBag.Workshops = db.Workshops.ToList();
            return View(viewModel);
        }

        [HttpGet]
        public ActionResult MachineStopsPerReason(MachineStopsPerReasonViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var query = db.MachineStops.Where(ms => ms.Date >= viewModel.StartDate && ms.Date <= viewModel.EndDate);
                var productionsQuery = db.Productions
                                            .Include(p => p.ProductionBatch.ProductCode)
                                            .AsNoTracking()
                                            .Where(p => p.Date >= viewModel.StartDate && p.Date <= viewModel.EndDate);

                if (viewModel.WorkshopLineId != null)
                {
                    query = query.Where(ms => ms.WorkshopLineId == viewModel.WorkshopLineId);
                    productionsQuery = productionsQuery.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
                } else if (viewModel.WorkshopId != null)
                {
                    query = query.Where(ms => ms.WorkshopLine.WorkshopId == viewModel.WorkshopLineId);
                    productionsQuery = productionsQuery.Where(p => p.WorkshopLine.WorkshopId == viewModel.WorkshopLineId);
                }

                if (viewModel.StartTime != null|| viewModel.EndTime != null)
                {
                    if (viewModel.StartTime == null)
                    {
                        query = query.Where(ms => ms.Finish == viewModel.EndTime);
                        productionsQuery = productionsQuery.Where(p => p.EndTime == viewModel.EndTime);
                    }
                    else if (viewModel.EndTime == null)
                    {
                        query = query.Where(ms => ms.Start == viewModel.StartTime);
                        productionsQuery = productionsQuery.Where(p => p.StartTime == viewModel.StartTime);
                    }
                    else
                    {
                        query = query.Where(ms => ms.Start >= viewModel.StartTime && ms.Finish <= viewModel.EndTime);
                        productionsQuery = productionsQuery.Where(p => p.StartTime >= viewModel.StartTime && p.EndTime == viewModel.EndTime);
                    }
                }

                viewModel.MachineStopsPerReasonData = new Flot.FlotDataCollection();
                foreach (var machineStopGroup in query.GroupBy(ms => ms.MachineStopReason).ToList())
                {
                    viewModel.MachineStopsPerReasonData.Add(new Flot.FlotData
                    {
                        X = machineStopGroup.Key.Name,
                        Y = machineStopGroup.Sum(ms => (ms.Finish - ms.Start).TotalHours)
                    });
                }

                var productions = productionsQuery.ToList();
                var realizedVolume = productions.Sum(p => p.RealQuantity * p.ProductionBatch.ProductCode.Eu);
                var targetVolume = productions.Sum(p => p.Target * p.ProductionBatch.ProductCode.Eu);
                viewModel.Oee = decimal.Round(Convert.ToDecimal(realizedVolume / (targetVolume > 0 ? targetVolume : 1)), 2 )*100;

                var last5Days = viewModel.StartDate.AddDays(-5);
                var productionsLast5Days = db.Productions.AsNoTracking().Where(p => p.Date >= last5Days && p.Date <= viewModel.StartDate);
                var productionSlots = productions.Count;
                // var availableSlots = db.TimeSuggestions.AsNoTracking().Count();
                var availableSlots = (120) - productionsLast5Days.Count();
                viewModel.Au = Convert.ToDecimal(productionSlots / availableSlots);
            }

            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLines = db.WorkshopLines.ToList();
            return View(viewModel);
        }

        [HttpGet]
        public async Task<JsonResult> MachineStopsPerReasonJson(MachineStopsPerReasonViewModel viewModel)
        {
            var query = db.MachineStops.Where(p => p.Date >= viewModel.StartDate && p.Date <= viewModel.EndDate);

            if (viewModel.WorkshopLineId != null && viewModel.WorkshopId != Guid.Empty)
            {
                query = query.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
            }

            if (viewModel.StartTime != null)
            {
                query = query.Where(p => p.Start == viewModel.StartTime);
            }

            if (viewModel.EndTime != null)
            {
                query = query.Where(p => p.Finish == viewModel.EndTime);
            }

            return Json(await query.Select(p => new
            {
                WorkshopLine = p.WorkshopLine.Name,
                Comment = p.Comment,
                Date = p.Date,
                Start = p.Start,
                Finish = p.Finish,
                MachineStopReason = p.MachineStopReason.Name,
                MachineStopMode = p.MachineStopReason.MachineStopMode.Name
            }).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ProductionInterval(ProductionIntervalViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var productionsQuery = db.Productions
                                            .Include(p => p.ProductionBatch.ProductCode)
                                            .AsNoTracking()
                                            .Where(p => p.Date >= viewModel.StartDate && p.Date <= viewModel.EndDate);

                if (viewModel.WorkshopLineId != null)
                {
                    productionsQuery = productionsQuery.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
                }

                if (viewModel.StartTime != null || viewModel.EndTime != null)
                {
                    if (viewModel.StartTime == null)
                    {
                        productionsQuery = productionsQuery.Where(p => p.EndTime == viewModel.EndTime);
                    }
                    else if (viewModel.EndTime == null)
                    {
                        productionsQuery = productionsQuery.Where(p => p.StartTime == viewModel.StartTime);
                    }
                    else
                    {
                        productionsQuery = productionsQuery.Where(p => p.StartTime >= viewModel.StartTime && p.EndTime == viewModel.EndTime);
                    }
                }

                viewModel.RealQuantities = new Flot.FlotDataCollection();
                viewModel.Targets = new Flot.FlotDataCollection();

                switch (viewModel.Group)
                {
                    case ProductionIntervalGroup.Hour:
                        foreach (var productionGroup in productionsQuery.OrderBy(p => p.Date).ThenBy(p => p.StartTime).ToList().GroupBy(p => p.Date.Add(p.StartTime)))
                        {
                            // var absDate = productionGroup.First().Date.Add(productionGroup.First().StartTime).Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                            var absDate = productionGroup.First().Date.Add(productionGroup.First().StartTime);
                            var absDateEnd = productionGroup.First().Date.Add(productionGroup.First().EndTime);
                            viewModel.RealQuantities.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM HH:mm") + "-" + absDateEnd.ToString("HH:mm"),
                                Y = productionGroup.Sum(p => p.RealQuantity)
                            });
                            viewModel.Targets.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM HH:mm") + "-" + absDateEnd.ToString("HH:mm"),
                                Y = productionGroup.Sum(p => p.Target)
                            });
                        }
                        break;
                    case ProductionIntervalGroup.Day:
                        foreach (var productionGroup in productionsQuery.ToList().OrderBy(p => p.Date).GroupBy(p => p.Date.Date))
                        {
                            // var absDate = productionGroup.First().Date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                            var absDate = productionGroup.First().Date;
                            viewModel.RealQuantities.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM/yyyy"),
                                Y = productionGroup.Sum(p => p.RealQuantity)
                            });
                            viewModel.Targets.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM/yyyy"),
                                Y = productionGroup.Sum(p => p.Target)
                            });
                        }
                        break;
                    case ProductionIntervalGroup.Week:
                        foreach (var productionGroup in productionsQuery.ToList().GroupBy(p => p.Date.StartOfWeek(DayOfWeek.Monday)))
                        {
                            // var absDate = productionGroup.Key.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
                            var absDate = productionGroup.Key;
                            viewModel.RealQuantities.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM/yyyy"),
                                Y = productionGroup.Sum(p => p.RealQuantity)
                            });
                            viewModel.Targets.Add(new Flot.FlotData
                            {
                                X = absDate.ToString("dd/MM/yyyy"),
                                Y = productionGroup.Sum(p => p.Target)
                            });
                        }
                        break;
                    case ProductionIntervalGroup.Month:
                        foreach (var productionGroup in productionsQuery.GroupBy(p => p.Date.Month).ToList())
                        {
                            viewModel.RealQuantities.Add(new Flot.FlotData
                            {
                                X = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(productionGroup.Key),
                                Y = productionGroup.Sum(p => p.RealQuantity)
                            });
                            viewModel.Targets.Add(new Flot.FlotData
                            {
                                X = System.Threading.Thread.CurrentThread.CurrentCulture.DateTimeFormat.GetMonthName(productionGroup.Key),
                                Y = productionGroup.Sum(p => p.Target)
                            });
                        }
                        break;
                }

            }

            ViewBag.Workshops = db.Workshops.ToList();
            ViewBag.WorkshopLines = db.WorkshopLines.ToList();
            return View(viewModel);
        }
    }
}