﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SopEmbraco.Models;

namespace SopEmbraco.Controllers
{
    public class MachineStopModesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MachineStopModes
        public async Task<ActionResult> Index()
        {
            return View(await db.MachineStopModes.OrderBy(m=> m.Name).ToListAsync());
        }

        // GET: MachineStopModes/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopMode machineStopMode = await db.MachineStopModes.FindAsync(id);
            if (machineStopMode == null)
            {
                return HttpNotFound();
            }
            return View(machineStopMode);
        }

        // GET: MachineStopModes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MachineStopModes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MachineStopModeId,Name")] MachineStopMode machineStopMode)
        {
            if (ModelState.IsValid)
            {
                machineStopMode.MachineStopModeId = Guid.NewGuid();
                db.MachineStopModes.Add(machineStopMode);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(machineStopMode);
        }

        // GET: MachineStopModes/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopMode machineStopMode = await db.MachineStopModes.FindAsync(id);
            if (machineStopMode == null)
            {
                return HttpNotFound();
            }
            return View(machineStopMode);
        }

        // POST: MachineStopModes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MachineStopModeId,Name")] MachineStopMode machineStopMode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(machineStopMode).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(machineStopMode);
        }

        // GET: MachineStopModes/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStopMode machineStopMode = await db.MachineStopModes.FindAsync(id);
            if (machineStopMode == null)
            {
                return HttpNotFound();
            }
            return View(machineStopMode);
        }

        // POST: MachineStopModes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            MachineStopMode machineStopMode = await db.MachineStopModes.FindAsync(id);
            db.MachineStopModes.Remove(machineStopMode);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<JsonResult> GetAllJson()
        {
            return Json(await db.MachineStopModes.OrderBy(m=> m.Name).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
