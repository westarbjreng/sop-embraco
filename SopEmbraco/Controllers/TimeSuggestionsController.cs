﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SopEmbraco.Models;

namespace SopEmbraco.Controllers
{
    public class TimeSuggestionsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: TimeSuggestions
        public async Task<ActionResult> Index()
        {
            return View(await db.TimeSuggestions.OrderBy(t=> t.StartTime).ToListAsync());
        }

        // GET: TimeSuggestions/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSuggestion timeSuggestion = await db.TimeSuggestions.FindAsync(id);
            if (timeSuggestion == null)
            {
                return HttpNotFound();
            }
            return View(timeSuggestion);
        }

        // GET: TimeSuggestions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TimeSuggestions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "TimeSuggestionId,StartTime,EndTime")] TimeSuggestion timeSuggestion)
        {
            if (ModelState.IsValid)
            {
                timeSuggestion.TimeSuggestionId = Guid.NewGuid();
                db.TimeSuggestions.Add(timeSuggestion);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(timeSuggestion);
        }

        // GET: TimeSuggestions/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSuggestion timeSuggestion = await db.TimeSuggestions.FindAsync(id);
            if (timeSuggestion == null)
            {
                return HttpNotFound();
            }
            return View(timeSuggestion);
        }

        // POST: TimeSuggestions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "TimeSuggestionId,StartTime,EndTime")] TimeSuggestion timeSuggestion)
        {
            if (ModelState.IsValid)
            {
                db.Entry(timeSuggestion).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(timeSuggestion);
        }

        // GET: TimeSuggestions/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TimeSuggestion timeSuggestion = await db.TimeSuggestions.FindAsync(id);
            if (timeSuggestion == null)
            {
                return HttpNotFound();
            }
            return View(timeSuggestion);
        }

        // POST: TimeSuggestions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            TimeSuggestion timeSuggestion = await db.TimeSuggestions.FindAsync(id);
            db.TimeSuggestions.Remove(timeSuggestion);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
