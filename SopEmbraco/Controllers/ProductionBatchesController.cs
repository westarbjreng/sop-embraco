﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SopEmbraco.Models;

namespace SopEmbraco.Controllers
{
    public class ProductionBatchesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ProductionBatches
        public async Task<ActionResult> Index()
        {
            return View(await db.ProductionBatches.OrderBy(p=> p.BatchNumber).ToListAsync());
        }

        // GET: ProductionBatches/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionBatch productionBatch = await db.ProductionBatches.FindAsync(id);
            if (productionBatch == null)
            {
                return HttpNotFound();
            }
            return View(productionBatch);
        }

        // GET: ProductionBatches/Create
        public ActionResult Create()
        {
            ViewBag.ProductCodeId = new SelectList(db.ProductCodes.OrderBy(pc=> pc.Name), "ProductCodeId", "Name");
            ViewBag.ProductModels = db.ProductModels.ToList();
            ViewBag.Workshops = db.Workshops.OrderBy(w=> w.Name).ToList();
            return View();
        }

        // POST: ProductionBatches/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProductionBatchId,ProductCodeId,ProductModelId,BatchNumber,Quantity")] ProductionBatch productionBatch)
        {
            if (ModelState.IsValid)
            {
                productionBatch.ProductionBatchId = Guid.NewGuid();
                db.ProductionBatches.Add(productionBatch);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ProductCodeId = new SelectList(db.ProductCodes, "ProductCodeId", "Name", productionBatch.ProductCodeId);
            ViewBag.ProductModels = db.ProductModels.ToList();
            ViewBag.Workshops = db.Workshops.ToList();
            return View(productionBatch);
        }

        // GET: ProductionBatches/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionBatch productionBatch = await db.ProductionBatches.Include(pb => pb.ProductCode).FirstOrDefaultAsync(pb => pb.ProductionBatchId == id);
            if (productionBatch == null)
            {
                return HttpNotFound();
            }

            productionBatch.WorkshopId = productionBatch.ProductCode.WorkshopId;
            productionBatch.ProductModelId = productionBatch.ProductCode.ProductModelId;

            ViewBag.ProductCodeId = new SelectList(db.ProductCodes, "ProductCodeId", "Name", productionBatch.ProductCodeId);
            ViewBag.ProductModels = db.ProductModels.ToList();
            ViewBag.Workshops = db.Workshops.ToList();
            return View(productionBatch);
        }

        // POST: ProductionBatches/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProductionBatchId,ProductCodeId,ProductModelId,BatchNumber,Quantity,Status")] ProductionBatch productionBatch)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productionBatch).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.ProductCodeId = new SelectList(db.ProductCodes, "ProductCodeId", "Name", productionBatch.ProductCodeId);
            ViewBag.ProductModels = db.ProductModels.ToList();
            ViewBag.Workshops = db.Workshops.ToList();
            return View(productionBatch);
        }

        // GET: ProductionBatches/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductionBatch productionBatch = await db.ProductionBatches.FindAsync(id);
            if (productionBatch == null)
            {
                return HttpNotFound();
            }
            return View(productionBatch);
        }

        // POST: ProductionBatches/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ProductionBatch productionBatch = await db.ProductionBatches.FindAsync(id);
            db.ProductionBatches.Remove(productionBatch);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public JsonResult GetProductCodeJson(Guid id)
        {
            var productionBatch = db.ProductionBatches.Include(pb => pb.ProductCode).FirstOrDefault(pb => pb.ProductionBatchId == id);
            return Json(new {
                Name = productionBatch.ProductCode.Name,
                Quantity = productionBatch.Quantity,
                Target = productionBatch.ProductCode.Target }, 
                JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> GetAllJson()
        {
            return Json(await db.ProductionBatches.ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> SelectByProductCodeId(Guid? id)
        {
            IQueryable<ProductionBatch> query = db.ProductionBatches;
            if (id != null) query = query.Where(c => c.ProductCodeId == id);
            var productCodes = await query.ToListAsync();
            return Json(productCodes.OrderBy(c => c.BatchNumber).ToList(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
