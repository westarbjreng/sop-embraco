﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SopEmbraco.Models;
using System.Configuration;
using System.IO;
using SopEmbraco.Infrastructure;

namespace SopEmbraco.Controllers
{
    public class WorkshopLinesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string directoryFiles = ConfigurationManager.AppSettings["DirectoryFiles"];
        private string domain = ConfigurationManager.AppSettings["Domain"];
        private string username = ConfigurationManager.AppSettings["Username"];
        private string password = ConfigurationManager.AppSettings["Password"];

        // GET: WorkshopLines
        public async Task<ActionResult> Index()
        {
            var workshopLines = db.WorkshopLines.Include(w => w.Workshop);
            return View(await workshopLines.OrderBy(wl => wl.Workshop.Name).ThenBy(wl => wl.Name).ToListAsync());
        }

        // GET: WorkshopLines/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkshopLine workshopLine = await db.WorkshopLines.FindAsync(id);
            if (workshopLine == null)
            {
                return HttpNotFound();
            }
            return View(workshopLine);
        }

        // GET: WorkshopLines/Create
        public ActionResult Create()
        {
            ViewBag.WorkshopId = new SelectList(db.Workshops.OrderBy(w=> w.Name), "WorkshopId", "Name");
            return View();
        }

        // POST: WorkshopLines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "WorkshopLineId,WorkshopId,Name,Positions")] WorkshopLine workshopLine)
        {
            if (ModelState.IsValid)
            {
                workshopLine.WorkshopLineId = Guid.NewGuid();
                db.WorkshopLines.Add(workshopLine);
                await db.SaveChangesAsync();

                var savedWorkshopLine = db.WorkshopLines.AsNoTracking().FirstOrDefault(p => p.WorkshopLineId == workshopLine.WorkshopLineId);
                // var path = Server.MapPath(directoryFiles + savedWorkshopLine.Workshop.Name);
                var path = Path.Combine(directoryFiles, savedWorkshopLine.Workshop.Name);

                var productCodes = savedWorkshopLine.Workshop.ProductCodes.ToList();
                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                foreach (var productCode in productCodes)
                {
                    // var eachPath = path + "/" + productCode.ProductModel.Name + "/" + productCode.Name + "/" + savedWorkshopLine.Name;
                    var eachPath = Path.Combine(path + "/" + productCode.ProductModel.Name + "/" + productCode.Name, savedWorkshopLine.Name);
                    context.Enter();
                    if (!System.IO.Directory.Exists(eachPath))
                        System.IO.Directory.CreateDirectory(eachPath);
                    context.Leave();
                }
                
                return RedirectToAction("Index");
            }

            ViewBag.WorkshopId = new SelectList(db.Workshops, "WorkshopId", "Name", workshopLine.WorkshopId);
            return View(workshopLine);
        }

        // GET: WorkshopLines/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkshopLine workshopLine = await db.WorkshopLines.FindAsync(id);
            if (workshopLine == null)
            {
                return HttpNotFound();
            }
            ViewBag.WorkshopId = new SelectList(db.Workshops.OrderBy(w=> w.Name), "WorkshopId", "Name", workshopLine.WorkshopId);
            return View(workshopLine);
        }

        // POST: WorkshopLines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "WorkshopLineId,WorkshopId,Name,Positions")] WorkshopLine workshopLine)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workshopLine).State = EntityState.Modified;
                await db.SaveChangesAsync();

                var savedWorkshopLine = db.WorkshopLines.AsNoTracking().FirstOrDefault(p => p.WorkshopLineId == workshopLine.WorkshopLineId);
                // var path = Server.MapPath(directoryFiles + savedWorkshopLine.Workshop.Name);
                var path = Path.Combine(directoryFiles, savedWorkshopLine.Workshop.Name);

                var productCodes = savedWorkshopLine.Workshop.ProductCodes.ToList();
                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                foreach (var productCode in productCodes)
                {
                    // var eachPath = path + "/" + productCode.ProductModel.Name + "/" + productCode.Name + "/" + savedWorkshopLine.Name;
                    var eachPath = Path.Combine(path + "/" + productCode.ProductModel.Name + "/" + productCode.Name, savedWorkshopLine.Name);
                    context.Enter();
                    if (!System.IO.Directory.Exists(eachPath))
                        System.IO.Directory.CreateDirectory(eachPath);
                    context.Leave();
                }

                return RedirectToAction("Index");
            }
            ViewBag.WorkshopId = new SelectList(db.Workshops, "WorkshopId", "Name", workshopLine.WorkshopId);
            return View(workshopLine);
        }

        // GET: WorkshopLines/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            WorkshopLine workshopLine = await db.WorkshopLines.FindAsync(id);
            if (workshopLine == null)
            {
                return HttpNotFound();
            }
            return View(workshopLine);
        }

        // POST: WorkshopLines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            WorkshopLine workshopLine = await db.WorkshopLines.FindAsync(id);
            db.WorkshopLines.Remove(workshopLine);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<JsonResult> SelectByWorkshopId(Guid? id)
        {
            var query = db.WorkshopLines.AsQueryable();
            if (id != null) query = query.Where(c => c.WorkshopId == id);
            var workshopLines = await query.OrderBy(c => c.Name).ToListAsync();
            return Json(workshopLines, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> Get(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            WorkshopLine workshopLine = await db.WorkshopLines.FindAsync(id);
            if (workshopLine == null)
            {
                return null;
            }

            return Json(workshopLine, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
