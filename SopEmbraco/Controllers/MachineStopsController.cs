﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SopEmbraco.Models;
using SopEmbraco.ViewModels;

namespace SopEmbraco.Controllers
{
    public class MachineStopsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: MachineStops
        public async Task<ActionResult> Index()
        {
            var machineStops = db.MachineStops.Include(m => m.MachineStopReason);
            return View(await machineStops.ToListAsync());
        }

        // GET: MachineStops/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStop machineStop = await db.MachineStops.FindAsync(id);
            if (machineStop == null)
            {
                return HttpNotFound();
            }
            return View(machineStop);
        }

        // GET: MachineStops/Create
        public ActionResult Create()
        {
            ViewBag.MachineStopReasonId = new SelectList(db.MachineStopReasons, "MachineStopReasonId", "Name");
            return View();
        }

        // POST: MachineStops/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "MachineStopId,MachineStopReasonId,Date,Start,Finish,Comment")] MachineStop machineStop)
        {
            if (ModelState.IsValid)
            {
                machineStop.MachineStopId = Guid.NewGuid();
                db.MachineStops.Add(machineStop);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.MachineStopReasonId = new SelectList(db.MachineStopReasons, "MachineStopReasonId", "Name", machineStop.MachineStopReasonId);
            return View(machineStop);
        }

        // GET: MachineStops/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStop machineStop = await db.MachineStops.FindAsync(id);
            if (machineStop == null)
            {
                return HttpNotFound();
            }
            ViewBag.MachineStopReasonId = new SelectList(db.MachineStopReasons, "MachineStopReasonId", "Name", machineStop.MachineStopReasonId);
            return View(machineStop);
        }

        // POST: MachineStops/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "MachineStopId,MachineStopReasonId,Date,Start,Finish,Comment")] MachineStop machineStop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(machineStop).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.MachineStopReasonId = new SelectList(db.MachineStopReasons, "MachineStopReasonId", "Name", machineStop.MachineStopReasonId);
            return View(machineStop);
        }

        // GET: MachineStops/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MachineStop machineStop = await db.MachineStops.FindAsync(id);
            if (machineStop == null)
            {
                return HttpNotFound();
            }
            return View(machineStop);
        }

        // POST: MachineStops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            MachineStop machineStop = await db.MachineStops.FindAsync(id);
            db.MachineStops.Remove(machineStop);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<JsonResult> IndexJson(MachineStopViewModel viewModel)
        {
            var query = db.MachineStops.AsQueryable();

            if (viewModel.WorkshopLineId != null)
            {
                query = query.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
            }

            if (viewModel.Date != null)
            {
                query = query.Where(p => p.Date == viewModel.Date);
            }

            return Json(await query.Select(p => new
            {
                WorkshopLineId = p.WorkshopLineId,
                MachineStopId = p.MachineStopId,
                Comment = p.Comment,
                Date = p.Date,
                Start = p.Start,
                Finish = p.Finish,
                MachineStopReasonId = p.MachineStopReasonId
            }).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> ReportJson(MachineStopReportViewModel viewModel)
        {
            var query = db.MachineStops.AsQueryable();

            if (viewModel.WorkshopLineId != null)
            {
                query = query.Where(p => p.WorkshopLineId == viewModel.WorkshopLineId);
            }

            if (viewModel.StartDate > DateTime.MinValue && viewModel.EndDate != DateTime.MinValue)
            {
                query = query.Where(p => p.Date >= viewModel.StartDate && p.Date <= viewModel.EndDate);
            }

            return Json(await query.Select(p => new
            {
                WorkshopLineId = p.WorkshopLineId,
                MachineStopId = p.MachineStopId,
                Comment = p.Comment,
                Date = p.Date,
                Start = p.Start,
                Finish = p.Finish,
                MachineStopReasonId = p.MachineStopReasonId
            }).ToListAsync(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> CreateJson([Bind(Include = "WorkshopLineId,MachineStopReasonId,Date,Start,Finish,Comment")] MachineStop machineStop)
        {
            if (ModelState.IsValid)
            {
                machineStop.MachineStopId = Guid.NewGuid();
                db.MachineStops.Add(machineStop);
                await db.SaveChangesAsync();
                return Json(machineStop, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Failure" }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public async Task<JsonResult> EditJson([Bind(Include = "WorkshopLineId,MachineStopId,MachineStopReasonId,Date,Start,Finish,Comment")] MachineStop machineStop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(machineStop).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Json(machineStop, JsonRequestBehavior.DenyGet);
            }

            return Json(new { Status = "Failure" }, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteJson([Bind(Include = "MachineStopId")] MachineStop machineStop)
        {
            var machineStopDb = await db.MachineStops.FirstOrDefaultAsync(ms => ms.MachineStopId == machineStop.MachineStopId);
            db.MachineStops.Remove(machineStopDb);
            await db.SaveChangesAsync();
            return Json(new { Status = "Ok" }, JsonRequestBehavior.DenyGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
