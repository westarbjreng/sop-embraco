﻿using System;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using SopEmbraco.Models;
using System.Configuration;
using System.IO;
using System.Security.Principal;
using SopEmbraco.Infrastructure;

namespace SopEmbraco.Controllers
{
    public class WorkshopsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        private string directoryFiles = ConfigurationManager.AppSettings["DirectoryFiles"];
        private string domain = ConfigurationManager.AppSettings["Domain"];
        private string username = ConfigurationManager.AppSettings["Username"];
        private string password = ConfigurationManager.AppSettings["Password"];

        // GET: Workshops
        public async Task<ActionResult> Index()
        {
            return View(await db.Workshops.OrderBy(pc => pc.Name).ToListAsync());
        }

        // GET: Workshops/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workshop workshop = await db.Workshops.FindAsync(id);
            if (workshop == null)
            {
                return HttpNotFound();
            }
            return View(workshop);
        }

        // GET: Workshops/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Workshops/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "WorkshopId,Name")] Workshop workshop)
        {
            if (ModelState.IsValid)
            {
                workshop.WorkshopId = Guid.NewGuid();
                db.Workshops.Add(workshop);
                await db.SaveChangesAsync();

                var savedWorkshop = db.Workshops.AsNoTracking().FirstOrDefault(p => p.WorkshopId == workshop.WorkshopId);
                // var path = Server.MapPath(directoryFiles + savedWorkshop.Name);
                var path = Path.Combine(directoryFiles, savedWorkshop.Name);

                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                context.Enter();
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                context.Leave();

                return RedirectToAction("Index");
            }

            return View(workshop);
        }

        // GET: Workshops/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workshop workshop = await db.Workshops.FindAsync(id);
            if (workshop == null)
            {
                return HttpNotFound();
            }
            return View(workshop);
        }

        // POST: Workshops/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "WorkshopId,Name")] Workshop workshop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(workshop).State = EntityState.Modified;
                await db.SaveChangesAsync();

                var savedWorkshop = db.Workshops.AsNoTracking().FirstOrDefault(p => p.WorkshopId == workshop.WorkshopId);
                // var path = Server.MapPath(directoryFiles + savedWorkshop.Name);
                var path = Path.Combine(directoryFiles, savedWorkshop.Name);

                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                context.Enter();
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                context.Leave();

                return RedirectToAction("Index");
            }
            return View(workshop);
        }

        // GET: Workshops/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Workshop workshop = await db.Workshops.FindAsync(id);
            if (workshop == null)
            {
                return HttpNotFound();
            }
            return View(workshop);
        }

        // POST: Workshops/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            Workshop workshop = await db.Workshops.FindAsync(id);
            db.Workshops.Remove(workshop);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        public async Task<JsonResult> Get(Guid? id)
        {
            if (id == null)
            {
                return null;
            }
            Workshop workshop = await db.Workshops.FindAsync(id);
            if (workshop == null)
            {
                return null;
            }

            return Json(workshop, JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
