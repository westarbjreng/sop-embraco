﻿using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using SopEmbraco.Models;
using System.Configuration;
using System.IO;
using SopEmbraco.Infrastructure;

namespace SopEmbraco.Controllers
{
    public class ProductCodesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        private string directoryFiles = ConfigurationManager.AppSettings["DirectoryFiles"];
        private string domain = ConfigurationManager.AppSettings["Domain"];
        private string username = ConfigurationManager.AppSettings["Username"];
        private string password = ConfigurationManager.AppSettings["Password"];

        // GET: ProductCodes
        public async Task<ActionResult> Index()
        {
            return View(await db.ProductCodes.OrderBy(pc => pc.Workshop.Name).ThenBy(pc => pc.Name).ToListAsync());
        }

        // GET: ProductCodes/Details/5
        public async Task<ActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCode productCode = await db.ProductCodes.FindAsync(id);
            if (productCode == null)
            {
                return HttpNotFound();
            }

            return View(productCode);
        }

        // GET: ProductCodes/Create
        public ActionResult Create()
        {
            ViewBag.WorkshopId = new SelectList(db.Workshops.OrderBy(w=> w.Name), "WorkshopId", "Name");
            ViewBag.ProductModelId = new SelectList(db.ProductModels.OrderBy(m=> m.Name), "ProductModelId", "Name");
            return View();
        }

        // POST: ProductCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ProductCodeId,Name,ProductModelId,WorkshopId,Target,Eu")] ProductCode productCode)
        {
            if (ModelState.IsValid)
            {
                productCode.ProductCodeId = Guid.NewGuid();
                db.ProductCodes.Add(productCode);
                await db.SaveChangesAsync();

                var savedProductCode = db.ProductCodes
                    .AsNoTracking()
                    .Include(pc => pc.Workshop)
                    .Include(pc => pc.ProductModel)
                    .FirstOrDefault(p => p.ProductCodeId == productCode.ProductCodeId);
                // var path = Server.MapPath(directoryFiles + savedProductCode.Workshop.Name + "/" + savedProductCode.ProductModel.Name + "/" + savedProductCode.Name);
                var path = Path.Combine(directoryFiles + "/" + savedProductCode.Workshop.Name + "/" + savedProductCode.ProductModel.Name, savedProductCode.Name);

                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                context.Enter();
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                context.Leave();

                return RedirectToAction("Index");
            }

            ViewBag.WorkshopId = new SelectList(db.Workshops, "WorkshopId", "Name", productCode.WorkshopId);
            ViewBag.ProductModelId = new SelectList(db.ProductModels, "ProductModelId", "Name", productCode.ProductModelId);
            return View(productCode);
        }

        // GET: ProductCodes/Edit/5
        public async Task<ActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCode productCode = await db.ProductCodes.FindAsync(id);
            if (productCode == null)
            {
                return HttpNotFound();
            }

            ViewBag.WorkshopId = new SelectList(db.Workshops.OrderBy(w=> w.Name), "WorkshopId", "Name", productCode.WorkshopId);
            ViewBag.ProductModelId = new SelectList(db.ProductModels.OrderBy(m=> m.Name), "ProductModelId", "Name", productCode.ProductModelId);
            return View(productCode);
        }

        // POST: ProductCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ProductCodeId,Name,ProductModelId,WorkshopId,Target,Eu")] ProductCode productCode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productCode).State = EntityState.Modified;
                await db.SaveChangesAsync();

                var savedProductCode = db.ProductCodes
                    .AsNoTracking()
                    .Include(pc => pc.Workshop)
                    .Include(pc => pc.ProductModel)
                    .FirstOrDefault(p => p.ProductCodeId == productCode.ProductCodeId);
                // var path = Server.MapPath(directoryFiles + savedProductCode.Workshop.Name + "/" + savedProductCode.ProductModel.Name + "/" + savedProductCode.Name);
                var path = Path.Combine(directoryFiles + "/" + savedProductCode.Workshop.Name + "/" + savedProductCode.ProductModel.Name, savedProductCode.Name);

                WrapperImpersonationContext context = new WrapperImpersonationContext(domain, username, password);
                context.Enter();
                if (!System.IO.Directory.Exists(path))
                    System.IO.Directory.CreateDirectory(path);
                context.Leave();

                return RedirectToAction("Index");
            }

            ViewBag.WorkshopId = new SelectList(db.Workshops, "WorkshopId", "Name", productCode.WorkshopId);
            ViewBag.ProductModelId = new SelectList(db.ProductModels, "ProductModelId", "Name", productCode.ProductModelId);
            return View(productCode);
        }

        // GET: ProductCodes/Delete/5
        public async Task<ActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCode productCode = await db.ProductCodes.FindAsync(id);
            if (productCode == null)
            {
                return HttpNotFound();
            }
            return View(productCode);
        }

        // POST: ProductCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(Guid id)
        {
            ProductCode productCode = await db.ProductCodes.FindAsync(id);
            db.ProductCodes.Remove(productCode);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public async Task<JsonResult> SelectByWorkshopId(Guid id, Guid? productModelId = null)
        {
            var query = db.ProductCodes.Where(c => c.WorkshopId == id);
            if (productModelId != null) query = query.Where(c => c.ProductModelId == productModelId);
            var productCodes = await query.ToListAsync();
            return Json(productCodes.OrderBy(c => c.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> SelectByProductModelId(Guid? id)
        {
            IQueryable<ProductCode> query = db.ProductCodes;
            if (id != null) query = query.Where(c => c.ProductModelId == id);
            var productCodes = await query.ToListAsync();
            return Json(productCodes.OrderBy(c => c.Name).ToList(), JsonRequestBehavior.AllowGet);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
