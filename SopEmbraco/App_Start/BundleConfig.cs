﻿using SopEmbraco.Infrastructure;
using System.Web;
using System.Web.Optimization;

namespace SopEmbraco
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            BundleTable.EnableOptimizations = false;

            bundles.Add(new ScriptBundle("~/bundles/carousel").Include(
                "~/Scripts/owl.carousel.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/charts").Include(
                "~/Scripts/Chart.js",
                "~/Scripts/chart_utils.js",
                "~/Scripts/flot/jquery.flot.js",
                "~/Scripts/flot/jquery.flot.categories.js",
                "~/Scripts/flot/jquery.flot.symbol.js",
                "~/Scripts/flot/jquery.flot.time.js",
                "~/Scripts/flot/jquery.flot.valuelabels.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/jquery.maskedinput.js",
                        "~/Scripts/moment.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js",
                        "~/Scripts/free-jqGrid/jquery.jqgrid.src.js"));

            var bundle = new ScriptBundle("~/bundles/jqueryval") { Orderer = new AsIsBundleOrderer() };

            bundle
                .Include("~/Scripts/jquery.validate.js")
                .Include("~/Scripts/jquery.validate.unobtrusive.js")
                .Include("~/Scripts/cldr.js")
                .Include("~/Scripts/globalize.js")
                .Include("~/Scripts/jquery.validate.globalize.js");
            bundles.Add(bundle);
            
            bundles.Add(new ScriptBundle("~/bundles/jsgrid").Include(
                        "~/Scripts/jsgrid.js",
                        "~/Scripts/jsgrid-customfields.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Content/Selectize/js/standalone/selectize.js"));

            bundles.Add(new StyleBundle("~/BundledContent/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/jsgrid.css",
                      "~/Content/jsgrid-theme.css",
                      "~/Content/OwlCarousel/owl.carousel.css",
                      "~/Content/OwlCarousel/owl.theme.css",
                      "~/Content/OwlCarousel/owl.transitions.css",
                      "~/Content/site.css",
                      "~/Content/ui.jqgrid.css",
                      "~/Content/ui.jqgrid-bootstrap.css",
                      "~/Content/themes/base/all.css",
                      "~/Content/Selectize/css/selectize.bootstrap3.css"));
        }
    }
}
