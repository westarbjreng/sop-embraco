﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SopEmbraco.Startup))]
namespace SopEmbraco
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
