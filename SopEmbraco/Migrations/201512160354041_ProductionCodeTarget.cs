namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductionCodeTarget : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCodes", "Target", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCodes", "Target");
        }
    }
}
