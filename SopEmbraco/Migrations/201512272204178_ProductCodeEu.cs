namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCodeEu : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductCodes", "Eu", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductCodes", "Eu");
        }
    }
}
