namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TimeSuggestions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TimeSuggestions",
                c => new
                    {
                        TimeSuggestionId = c.Guid(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.TimeSuggestionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TimeSuggestions");
        }
    }
}
