namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCodes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductCodes",
                c => new
                    {
                        ProductCodeId = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProductCodeId);
            
            CreateTable(
                "dbo.ProductCodeWorkshops",
                c => new
                    {
                        ProductCodeWorkshopId = c.Guid(nullable: false),
                        ProductCodeId = c.Guid(nullable: false),
                        WorkshopId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ProductCodeWorkshopId)
                .ForeignKey("dbo.ProductCodes", t => t.ProductCodeId, cascadeDelete: true)
                .ForeignKey("dbo.Workshops", t => t.WorkshopId, cascadeDelete: true)
                .Index(t => t.ProductCodeId)
                .Index(t => t.WorkshopId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductCodeWorkshops", "WorkshopId", "dbo.Workshops");
            DropForeignKey("dbo.ProductCodeWorkshops", "ProductCodeId", "dbo.ProductCodes");
            DropIndex("dbo.ProductCodeWorkshops", new[] { "WorkshopId" });
            DropIndex("dbo.ProductCodeWorkshops", new[] { "ProductCodeId" });
            DropTable("dbo.ProductCodeWorkshops");
            DropTable("dbo.ProductCodes");
        }
    }
}
