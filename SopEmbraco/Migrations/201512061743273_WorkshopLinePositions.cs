namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkshopLinePositions : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WorkshopLines", "Positions", c => c.Int(nullable: false));
            DropColumn("dbo.Workshops", "Positions");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Workshops", "Positions", c => c.Int(nullable: false));
            DropColumn("dbo.WorkshopLines", "Positions");
        }
    }
}
