namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductionBatchStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductionBatches", "Status", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ProductionBatches", "Status");
        }
    }
}
