namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MachineStopWorkshopLine : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MachineStops", "WorkshopLineId", c => c.Guid(nullable: false));
            CreateIndex("dbo.MachineStops", "WorkshopLineId");
            AddForeignKey("dbo.MachineStops", "WorkshopLineId", "dbo.WorkshopLines", "WorkshopLineId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MachineStops", "WorkshopLineId", "dbo.WorkshopLines");
            DropIndex("dbo.MachineStops", new[] { "WorkshopLineId" });
            DropColumn("dbo.MachineStops", "WorkshopLineId");
        }
    }
}
