namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductModelAndWorkshopPositions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ProductModels",
                c => new
                    {
                        ProductModelId = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ProductModelId);
            
            AddColumn("dbo.ProductCodes", "ProductModelId", c => c.Guid());
            AddColumn("dbo.Workshops", "Positions", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductCodes", "ProductModelId");
            AddForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels", "ProductModelId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels");
            DropIndex("dbo.ProductCodes", new[] { "ProductModelId" });
            DropColumn("dbo.Workshops", "Positions");
            DropColumn("dbo.ProductCodes", "ProductModelId");
            DropTable("dbo.ProductModels");
        }
    }
}
