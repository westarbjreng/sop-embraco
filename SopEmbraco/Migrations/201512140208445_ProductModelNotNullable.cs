namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductModelNotNullable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels");
            DropIndex("dbo.ProductCodes", new[] { "ProductModelId" });
            AlterColumn("dbo.ProductCodes", "ProductModelId", c => c.Guid(nullable: false));
            CreateIndex("dbo.ProductCodes", "ProductModelId");
            AddForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels", "ProductModelId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels");
            DropIndex("dbo.ProductCodes", new[] { "ProductModelId" });
            AlterColumn("dbo.ProductCodes", "ProductModelId", c => c.Guid());
            CreateIndex("dbo.ProductCodes", "ProductModelId");
            AddForeignKey("dbo.ProductCodes", "ProductModelId", "dbo.ProductModels", "ProductModelId");
        }
    }
}
