namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MachineStopModes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MachineStopModes",
                c => new
                    {
                        MachineStopModeId = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.MachineStopModeId);
            
            AddColumn("dbo.MachineStopReasons", "MachineStopModeId", c => c.Guid());
            CreateIndex("dbo.MachineStopReasons", "MachineStopModeId");
            AddForeignKey("dbo.MachineStopReasons", "MachineStopModeId", "dbo.MachineStopModes", "MachineStopModeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MachineStopReasons", "MachineStopModeId", "dbo.MachineStopModes");
            DropIndex("dbo.MachineStopReasons", new[] { "MachineStopModeId" });
            DropColumn("dbo.MachineStopReasons", "MachineStopModeId");
            DropTable("dbo.MachineStopModes");
        }
    }
}
