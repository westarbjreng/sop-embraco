namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCodeBelongsToWorkshop : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ProductCodeWorkshops", "ProductCodeId", "dbo.ProductCodes");
            DropIndex("dbo.ProductCodeWorkshops", new[] { "ProductCodeId" });
            DropIndex("dbo.ProductCodeWorkshops", new[] { "WorkshopId" });
            AddColumn("dbo.ProductCodes", "WorkshopId", c => c.Guid(nullable: false));
            CreateIndex("dbo.ProductCodes", "WorkshopId");
            DropTable("dbo.ProductCodeWorkshops");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ProductCodeWorkshops",
                c => new
                    {
                        ProductCodeWorkshopId = c.Guid(nullable: false),
                        ProductCodeId = c.Guid(nullable: false),
                        WorkshopId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.ProductCodeWorkshopId);
            
            DropIndex("dbo.ProductCodes", new[] { "WorkshopId" });
            DropColumn("dbo.ProductCodes", "WorkshopId");
            CreateIndex("dbo.ProductCodeWorkshops", "WorkshopId");
            CreateIndex("dbo.ProductCodeWorkshops", "ProductCodeId");
            AddForeignKey("dbo.ProductCodeWorkshops", "ProductCodeId", "dbo.ProductCodes", "ProductCodeId", cascadeDelete: true);
        }
    }
}
