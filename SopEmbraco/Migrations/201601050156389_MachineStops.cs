namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MachineStops : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MachineStopReasons",
                c => new
                    {
                        MachineStopReasonId = c.Guid(nullable: false),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.MachineStopReasonId);
            
            CreateTable(
                "dbo.MachineStops",
                c => new
                    {
                        MachineStopId = c.Guid(nullable: false),
                        MachineStopReasonId = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        Start = c.Time(nullable: false, precision: 7),
                        Finish = c.Time(nullable: false, precision: 7),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.MachineStopId)
                .ForeignKey("dbo.MachineStopReasons", t => t.MachineStopReasonId, cascadeDelete: true)
                .Index(t => t.MachineStopReasonId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MachineStops", "MachineStopReasonId", "dbo.MachineStopReasons");
            DropIndex("dbo.MachineStops", new[] { "MachineStopReasonId" });
            DropTable("dbo.MachineStops");
            DropTable("dbo.MachineStopReasons");
        }
    }
}
