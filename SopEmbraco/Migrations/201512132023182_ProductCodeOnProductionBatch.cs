namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductCodeOnProductionBatch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ProductionBatches", "ProductCodeId", c => c.Guid(nullable: true));
            AddColumn("dbo.ProductionBatches", "BatchNumber", c => c.Int(nullable: false));
            CreateIndex("dbo.ProductionBatches", "ProductCodeId");
            AddForeignKey("dbo.ProductionBatches", "ProductCodeId", "dbo.ProductCodes", "ProductCodeId", cascadeDelete: true);
            DropColumn("dbo.ProductionBatches", "ProductionCode");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductionBatches", "ProductionCode", c => c.String(nullable: false));
            DropForeignKey("dbo.ProductionBatches", "ProductCodeId", "dbo.ProductCodes");
            DropIndex("dbo.ProductionBatches", new[] { "ProductCodeId" });
            DropColumn("dbo.ProductionBatches", "BatchNumber");
            DropColumn("dbo.ProductionBatches", "ProductCodeId");
        }
    }
}
