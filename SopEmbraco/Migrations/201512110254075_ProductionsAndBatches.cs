namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductionsAndBatches : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Productions",
                c => new
                    {
                        ProductionId = c.Guid(nullable: false),
                        ProductionBatchId = c.Guid(nullable: false),
                        WorkshopLineId = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        Target = c.Int(nullable: false),
                        RealQuantity = c.Int(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.ProductionId)
                .ForeignKey("dbo.ProductionBatches", t => t.ProductionBatchId, cascadeDelete: true)
                .ForeignKey("dbo.WorkshopLines", t => t.WorkshopLineId, cascadeDelete: true)
                .Index(t => t.ProductionBatchId)
                .Index(t => t.WorkshopLineId);
            
            CreateTable(
                "dbo.ProductionBatches",
                c => new
                    {
                        ProductionBatchId = c.Guid(nullable: false),
                        ProductionCode = c.String(nullable: false),
                        Quantity = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ProductionBatchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Productions", "WorkshopLineId", "dbo.WorkshopLines");
            DropForeignKey("dbo.Productions", "ProductionBatchId", "dbo.ProductionBatches");
            DropIndex("dbo.Productions", new[] { "WorkshopLineId" });
            DropIndex("dbo.Productions", new[] { "ProductionBatchId" });
            DropTable("dbo.ProductionBatches");
            DropTable("dbo.Productions");
        }
    }
}
