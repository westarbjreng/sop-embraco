namespace SopEmbraco.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductionBatchBatchNumber : DbMigration
    {
        public override void Up()
        {
            Sql("alter table dbo.ProductionBatches drop constraint DF__Productio__Batch__6EF57B66;");
            AlterColumn("dbo.ProductionBatches", "BatchNumber", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ProductionBatches", "BatchNumber", c => c.Int(nullable: false));
        }
    }
}
