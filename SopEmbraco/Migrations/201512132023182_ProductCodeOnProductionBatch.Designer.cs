// <auto-generated />
namespace SopEmbraco.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class ProductCodeOnProductionBatch : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ProductCodeOnProductionBatch));
        
        string IMigrationMetadata.Id
        {
            get { return "201512132023182_ProductCodeOnProductionBatch"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
