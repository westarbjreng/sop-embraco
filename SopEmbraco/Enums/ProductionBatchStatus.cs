﻿namespace SopEmbraco.Enums
{
    public enum ProductionBatchStatus
    {
        Open,
        Concluded
    }
}