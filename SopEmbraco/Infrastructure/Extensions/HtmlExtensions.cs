﻿using SopEmbraco.Flot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SopEmbraco.Infrastructure.Extensions
{
    public static class HtmlExtensions
    {
        public static MvcHtmlString CreateFlotData(this HtmlHelper htmlHelper, FlotDataCollection dataCollection)
        {
            return CreateFlotData(htmlHelper, new Dictionary<string, FlotDataCollection> {
                { "data", dataCollection }
            });
        }

        public static MvcHtmlString CreateFlotData(this HtmlHelper htmlHelper, IDictionary<string, FlotDataCollection> dataCollectionList)
        {
            // return CreateFlotData(dataCollection.ToJson());
            var tag = new TagBuilder("script");
            tag.Attributes.Add("type", "text/javascript");
            var stringBuilder = new StringBuilder();
            foreach (var entry in dataCollectionList)
            {
                stringBuilder.AppendFormat("var {0} = JSON.parse('{1}');", entry.Key, "[" + string.Join(", ", entry.Value.Select(d => d.ToString()).ToArray()) + "]");
                // stringBuilder.Append(CreateFlotData("[" + string.Join(", ", entry.Value.Select(d => d.ToString()).ToArray()) + "]"));
            }

            tag.InnerHtml = stringBuilder.ToString();
            return new MvcHtmlString(tag.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="canvasId"></param>
        /// <param name="chartType"></param>
        /// <param name="jsonData"></param>
        /// <param name="jsonOptions"></param>
        /// <returns></returns>
        private static MvcHtmlString CreateFlotData(string jsonData)
        {
            var tag = new TagBuilder("script");
            tag.Attributes.Add("type", "text/javascript");
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendFormat("var data = JSON.parse('{0}');", jsonData);
            tag.InnerHtml = stringBuilder.ToString();
            return new MvcHtmlString(tag.ToString());
        }
    }
}