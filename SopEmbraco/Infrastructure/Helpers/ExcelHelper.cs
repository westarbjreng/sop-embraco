﻿using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace SopEmbraco.Infrastructure.Helpers
{
    public static class ExcelHelper
    {
        public static byte[] ExportList(IEnumerable list, String spreadsheetTitle = "Excel Export")
        {
            using (var excelPackage = new ExcelPackage())
            {
                excelPackage.Workbook.Properties.Author = "SOP Embraco";
                excelPackage.Workbook.Properties.Title = spreadsheetTitle;
                var sheet = excelPackage.Workbook.Worksheets.Add(spreadsheetTitle);
                sheet.Name = spreadsheetTitle;

                WriteWorksheet(list, sheet);

                return excelPackage.GetAsByteArray();
            }
        }

        public static void WriteWorksheet(IEnumerable list, ExcelWorksheet sheet)
        {
            var enumerator = list.GetEnumerator();
            enumerator.MoveNext();
            var type = enumerator.Current.GetType();
            var properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                                   BindingFlags.NonPublic | BindingFlags.FlattenHierarchy).ToList();
            var titles = properties.Select(p => p.Name).ToArray();
            var i = 1;
            foreach (var title in titles)
            {
                sheet.Cells[1, i++].Value = title;
            }

            var rowIndex = 2;
            foreach (var item in list)
            {
                var col = 1;
                foreach (var prop in properties)
                {
                    var val = item.GetType().GetProperty(prop.Name).GetValue(item, null);
                    String str;

                    if (val == null)
                    {
                        str = "";
                    }
                    else if (val.GetType().IsPrimitive || val.GetType() == typeof(Decimal) || val.GetType() == typeof(String) || val.GetType() == typeof(DateTime))
                    {
                        str = val.ToString();
                    }
                    else
                    {
                        if (val.GetType().GetProperty("Name") != null)
                        {
                            str = val.GetType().GetProperty("Name").GetValue(val, null).ToString();
                        }
                        else if (val.GetType().GetProperty("Description") != null)
                        {
                            str = val.GetType().GetProperty("Description").GetValue(val, null).ToString();
                        }
                        else
                        {
                            str = val.ToString();
                        }
                    }

                    sheet.Cells[rowIndex, col++].Value = str ?? "";
                }

                rowIndex++;
            }
        }

        private static void WriteWorksheet<T>(IEnumerable<T> list, ExcelWorksheet sheet)
            where T : class, new()
        {
            var properties = typeof(T).GetType().GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public |
                                   BindingFlags.NonPublic | BindingFlags.FlattenHierarchy);
            var titles = properties.Select(p => p.Name).ToArray();
            var i = 1;
            foreach (var title in titles)
            {
                sheet.Cells[1, i++].Value = title;
            }

            var rowIndex = 2;
            foreach (var item in list)
            {
                var col = 1;
                foreach (var prop in properties.Where(p => !p.Name.EndsWith("Id") && !p.PropertyType.IsGenericType))
                {
                    var val = item.GetType().GetProperty(prop.Name).GetValue(item, null);
                    String str;

                    if (val == null)
                    {
                        str = "";
                    }
                    else if (val.GetType().IsPrimitive || val.GetType() == typeof(Decimal) || val.GetType() == typeof(String) || val.GetType() == typeof(DateTime))
                    {
                        str = val.ToString();
                    }
                    else
                    {
                        if (val.GetType().GetProperty("Name") != null)
                        {
                            str = val.GetType().GetProperty("Name").GetValue(val, null).ToString();
                        }
                        else if (val.GetType().GetProperty("Description") != null)
                        {
                            str = val.GetType().GetProperty("Description").GetValue(val, null).ToString();
                        }
                        else
                        {
                            str = val.ToString();
                        }
                    }

                    sheet.Cells[rowIndex, col++].Value = str ?? "";
                }

                rowIndex++;
            }
        }
    }
}