﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SopEmbraco.Models
{
    public class Production : IValidatableObject
    {
        [Key]
        public Guid ProductionId { get; set; }
        public Guid ProductionBatchId { get; set; }
        public Guid WorkshopLineId { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; } = DateTime.Now;
        [Required]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan StartTime { get; set; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:hh\\:mm}", ApplyFormatInEditMode = true)]
        public TimeSpan EndTime { get; set; }
        [Required]
        public int Target { get; set; }
        [Required]
        public int RealQuantity { get; set; }
        [DataType(DataType.MultilineText)]
        public String Comment { get; set; }

        [NotMapped]
        public String ProductCodeName { get; set; }
        [NotMapped]
        public String PlanQuantity { get; set; }
        [NotMapped]
        public bool ProductionBatchStatusConcluded { get; set; }
        [NotMapped]
        public String ConcludedBatchName { get; set; }

        public virtual ProductionBatch ProductionBatch { get; set; }
        public virtual WorkshopLine WorkshopLine { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (RealQuantity < Target && String.IsNullOrEmpty(Comment))
            {
                yield return new ValidationResult("Please justify the difference between the amount of Target and Real Quantity produced.",
                    new[] { "Comment" });
            }
        }
    }
}
