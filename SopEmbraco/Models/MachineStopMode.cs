﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    public class MachineStopMode
    {
        [Key]
        public Guid MachineStopModeId { get; set; }

        [Required]
        public String Name { get; set; }

        public virtual ICollection<MachineStopReason> Reasons { get; set; }
    }
}