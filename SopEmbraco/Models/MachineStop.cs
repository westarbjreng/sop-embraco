﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    public class MachineStop
    {
        [Key]
        public Guid MachineStopId { get; set; }
        public Guid MachineStopReasonId { get; set; }
        public Guid WorkshopLineId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        [Required]
        public TimeSpan Start { get; set; }
        [Required]
        public TimeSpan Finish { get; set; }
        [DataType(DataType.MultilineText)]
        public String Comment { get; set; }

        [NotMapped]
        public Guid MachineStopModeId { get; set; }

        public virtual WorkshopLine WorkshopLine { get; set; }
        public virtual MachineStopReason MachineStopReason { get; set; }
    }
}