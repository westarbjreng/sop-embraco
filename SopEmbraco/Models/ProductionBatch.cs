﻿using Newtonsoft.Json;
using SopEmbraco.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SopEmbraco.Models
{
    public class ProductionBatch
    {
        [Key]
        public Guid ProductionBatchId { get; set; }
        public Guid ProductCodeId { get; set; }

        [Required]
        public String BatchNumber { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public ProductionBatchStatus Status { get; set; }

        [NotMapped]
        public Guid WorkshopId { get; set; }
        [NotMapped]
        public Guid ProductModelId { get; set; }

        public virtual ProductCode ProductCode { get; set; }
        [JsonIgnore]
        public virtual ICollection<Production> Productions { get; set; }
    }
}
