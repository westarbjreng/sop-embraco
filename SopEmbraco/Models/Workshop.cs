﻿using Newtonsoft.Json;
using SopEmbraco.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    [DisplayColumn("Name")]
    public class Workshop
    {
        [Key]
        public Guid WorkshopId { get; set; }
        [Required]
        [Unique(ErrorMessage = "Workshop Name must be unique.")]
        [DisplayName("Workshop")]
        public String Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<WorkshopLine> WorkshopLines { get; set; }
        [JsonIgnore]
        public virtual ICollection<ProductCode> ProductCodes { get; set; }
    }
}