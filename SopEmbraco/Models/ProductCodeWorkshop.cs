﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    public class ProductCodeWorkshop
    {
        [Key]
        public Guid ProductCodeWorkshopId { get; set; }
        public Guid ProductCodeId { get; set; }
        public Guid WorkshopId { get; set; }

        public virtual ProductCode ProductCode { get; set; }
        public virtual Workshop Workshop { get; set; }
    }
}