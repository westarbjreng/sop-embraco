﻿using Newtonsoft.Json;
using SopEmbraco.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SopEmbraco.Models
{
    public class ProductModel
    {
        [Key]
        public Guid ProductModelId { get; set; }
        [Required]
        [Unique(ErrorMessage = "Product Model Name must be unique.")]
        [DisplayName("Product Model")]
        public String Name { get; set; }

        [JsonIgnore]
        public virtual ICollection<ProductCode> ProductCodes { get; set; }
    }
}
