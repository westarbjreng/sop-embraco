﻿using Newtonsoft.Json;
using SopEmbraco.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    [DisplayColumn("Name")]
    public class WorkshopLine
    {
        [Key]
        public Guid WorkshopLineId { get; set; }
        public Guid WorkshopId { get; set; }

        [Required]
        [Unique(ErrorMessage = "Workshop Line Name must be unique.")]
        [DisplayName("Workshop Line")]
        public String Name { get; set; }
        [Required]
        [Range(1, Int32.MaxValue)]
        public int Positions { get; set; }

        public virtual Workshop Workshop { get; set; }

        [JsonIgnore]
        public virtual ICollection<Production> Productions { get; set; }
        [JsonIgnore]
        public virtual ICollection<MachineStop> MachineStops { get; set; }
    }
}