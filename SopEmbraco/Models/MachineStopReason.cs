﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SopEmbraco.Models
{
    public class MachineStopReason
    {
        [Key]
        public Guid MachineStopReasonId { get; set; }
        public Guid? MachineStopModeId { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public String Name { get; set; }

        public virtual MachineStopMode MachineStopMode { get; set; }

        [JsonIgnore]
        public virtual ICollection<MachineStop> MachineStops { get; set; }
    }
}