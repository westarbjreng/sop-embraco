﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SopEmbraco.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Workshop> Workshops { get; set; }
        public DbSet<WorkshopLine> WorkshopLines { get; set; }
        public DbSet<ProductCode> ProductCodes { get; set; }
        public DbSet<ProductModel> ProductModels { get; set; }
        public DbSet<Production> Productions { get; set; }
        public DbSet<ProductionBatch> ProductionBatches { get; set; }
        public DbSet<TimeSuggestion> TimeSuggestions { get; set; }
        public DbSet<MachineStop> MachineStops { get; set; }
        public DbSet<MachineStopReason> MachineStopReasons { get; set; }

        public System.Data.Entity.DbSet<SopEmbraco.Models.MachineStopMode> MachineStopModes { get; set; }
    }
}