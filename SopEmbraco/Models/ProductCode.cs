﻿using Newtonsoft.Json;
using SopEmbraco.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.Models
{
    [DisplayColumn("Name")]
    public class ProductCode
    {
        [Key]
        public Guid ProductCodeId { get; set; }
        public Guid ProductModelId { get; set; }
        public Guid WorkshopId { get; set; }

        [Required]
        [Unique(ErrorMessage = "Product Code Name must be unique.")]
        [DisplayName("Product Code")]
        public String Name { get; set; }

        [Required]
        public int Target { get; set; }
        [DisplayName("EU")]
        public decimal Eu { get; set; }

        public virtual ProductModel ProductModel { get; set; }
        public virtual Workshop Workshop { get; set; }

        [JsonIgnore]
        public virtual ICollection<ProductionBatch> ProductionBatches { get; set; }
    }
}