﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class FileViewModel
    {
        public String Name { get; set; }
        public Guid WorkshopLineId { get; set; }
        public Guid ProductCodeId { get; set; }
    }
}