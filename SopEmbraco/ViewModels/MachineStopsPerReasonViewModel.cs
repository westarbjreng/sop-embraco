﻿using System;
using System.Collections.Generic;
using SopEmbraco.Models;

namespace SopEmbraco.ViewModels
{
    public enum MachineStopsPerReasonGroup
    {
        Hour,
        Day,
        Week,
        Month
    }

    public class MachineStopsPerReasonViewModel
    {
        public Guid? WorkshopId { get; set; }
        public Guid? WorkshopLineId { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Today;
        public DateTime EndDate { get; set; } = DateTime.Today;
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public MachineStopsPerReasonGroup Group { get; set; }

        public Flot.FlotDataCollection MachineStopsPerReasonData { get; set; }
        public ICollection<Production> Productions { get; set; }
        public Decimal Oee { get; set; }
        public Decimal Au { get; set; }
    }
}