﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SopEmbraco.ViewModels
{
    public class ProductionsIndexViewModel
    {
        public Guid? WorkshopId { get; set; }
        public Guid? WorkshopLineId { get; set; }

        public virtual IEnumerable<SopEmbraco.Models.WorkshopLine> WorkshopLines { get; set; }
    }
}
