﻿using SopEmbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class ReportsWorkshopLineViewModel
    {
        public WorkshopLine WorkshopLine { get; set; }
        public ProductionBatch ProductionBatch { get; set; }

        public Flot.FlotDataCollection RealQuantities { get; set; }
        public Flot.FlotDataCollection Targets { get; set; }
    }
}