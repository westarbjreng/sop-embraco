﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class MachineStopViewModel
    {
        public Guid? WorkshopLineId { get; set; }
        [DataType(DataType.Date)]
        public DateTime? Date { get; set; }
    }
}