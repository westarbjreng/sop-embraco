﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class DigitalSopViewModel
    {
        [Required]
        public Guid WorkshopId { get; set; }
        public Guid? ProductModelId { get; set; }
        [Required]
        public Guid ProductCodeId { get; set; }
        [Required]
        public Guid WorkshopLineId { get; set; }
        public int? Position { get; set; }

        public List<FileViewModel> Files { get; set; } = new List<FileViewModel>();
    }
}