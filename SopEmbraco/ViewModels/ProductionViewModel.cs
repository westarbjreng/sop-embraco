﻿using SopEmbraco.Models;
using System;
using System.Collections.Generic;

namespace SopEmbraco.ViewModels
{
    public class ProductionViewModel
    {
        public Guid WorkshopId { get; set; }
        public Guid WorkshopLineId { get; set; }
        public Guid? ProductCodeId { get; set; }
        public Guid? ProductModelId { get; set; }
        public Guid? ProductionBatchId { get; set; }

        public DateTime? ProductionDate { get; set; }

        public virtual ICollection<Production> ConcludedProductions { get; set; }
        public virtual ICollection<Production> PendingProductions { get; set; }
        public virtual WorkshopLine WorkshopLine { get; set; }
    }
}
