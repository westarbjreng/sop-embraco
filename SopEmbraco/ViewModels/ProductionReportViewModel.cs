﻿using System;

namespace SopEmbraco.ViewModels
{
    public class ProductionReportViewModel
    {
        public Guid? WorkshopId { get; set; }
        public Guid? WorkshopLineId { get; set; }
        public Guid? ProductCodeId { get; set; }
        public Guid? ProductModelId { get; set; }
        public Guid? ProductionBatchId { get; set; }

        public DateTime ProductionStartDate { get; set; }
        public DateTime ProductionEndDate { get; set; }
    }
}
