﻿using SopEmbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class ProductionResultViewModel
    {
        public bool Valid { get; set; }
        public Production Production { get; set; }
    }
}