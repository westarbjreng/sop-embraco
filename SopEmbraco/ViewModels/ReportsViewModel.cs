﻿using SopEmbraco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public class ReportsViewModel
    {
        public Guid WorkshopId { get; set; }
        public DateTime Date { get; set; }

        public virtual ICollection<ReportsWorkshopLineViewModel> WorkshopLineReports { get; set; }
    }
}