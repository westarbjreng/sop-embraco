﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SopEmbraco.ViewModels
{
    public enum ProductionIntervalGroup
    {
        Hour, 
        Day,
        Week,
        Month
    }

    public class ProductionIntervalViewModel
    {
        public Guid? WorkshopId { get; set; }
        public Guid? WorkshopLineId { get; set; }
        public DateTime StartDate { get; set; } = DateTime.Today;
        public DateTime EndDate { get; set; } = DateTime.Today;
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public ProductionIntervalGroup Group { get; set; }

        public Flot.FlotDataCollection RealQuantities { get; set; }
        public Flot.FlotDataCollection Targets { get; set; }
    }
}