﻿var MyDateField = function (config) {
    jsGrid.Field.call(this, config);
};

var MyTimeField = function (config) {
    jsGrid.Field.call(this, config);
};

var ReadOnlyField = function (config) {
    jsGrid.Field.call(this, config);
};

var SelectWithClassField = function (config) {
    jsGrid.Field.call(this, config);
};

/* var TextWithClassField = function (config) {
    jsGrid.Field.call(this, config);
}; */

var NumberWithClassField = function (config) {
    jsGrid.Field.call(this, config);
};

MyDateField.prototype = new jsGrid.Field({
    css: "date-field",
    align: "center",

    sorter: function (date1, date2) {
        return new Date(date1) - new Date(date2);
    },

    itemTemplate: function (value) {
        return moment(value).format("DD/MM/YYYY");
    },

    insertTemplate: function (value) {
        if (!this.inserting)
            return "";

        var $result = this.insertControl = $("<input>").attr("type", "date");
        return $result;
    },

    editTemplate: function (value) {
        if (!this.editing)
            return this.itemTemplate(value);

        var $result = this.editControl = $("<input>").attr("type", "date");
        $result.val(moment(value).format("YYYY-MM-DD"));
        // $result.valueAsDate = new Date(value);
        return $result;
    },

    insertValue: function () {
        return this.insertControl.val();
    },

    editValue: function () {
        return this.editControl.val();
    },

    _createTextBox: function () {
        return $("<input>").attr("type", "date");
    },

    filterTemplate: function () {
        if (!this.filtering)
            return "";

        var grid = this._grid,
            $result = this.filterControl = this._createTextBox();

        if (this.autosearch) {
            $result.on("keypress", function (e) {
                if (e.which === 13) {
                    grid.search();
                    e.preventDefault();
                }
            });
        }

        return $result;
    }
});

MyTimeField.prototype = new jsGrid.Field({
    css: "time-field",

    insertTemplate: function (value) {
        if (!this.inserting)
            return "";

        var $result = this.insertControl = $("<input>").mask("99:99");
        return $result;
    },

    editTemplate: function (value) {
        if (!this.editing)
            return this.itemTemplate(value);

        var $result = this.editControl = $("<input>").mask("99:99");
        $result.val(value);
        return $result;
    },

    insertValue: function () {
        return this.insertControl.val();
    },

    editValue: function () {
        return this.editControl.val();
    },

    _createTextBox: function () {
        return $("<input>").attr("type", "text");
    },

    filterTemplate: function () {
        if (!this.filtering)
            return "";

        var grid = this._grid,
            $result = this.filterControl = this._createTextBox();

        $result.mask("99:99");

        if (this.autosearch) {
            $result.on("keypress", function (e) {
                if (e.which === 13) {
                    grid.search();
                    e.preventDefault();
                }
            });
        }

        return $result;
    }
});

ReadOnlyField.prototype = new jsGrid.Field({

    autosearch: true,

    filterTemplate: function () {
        if (!this.filtering)
            return "";

        var grid = this._grid,
            $result = this.filterControl = this._createTextBox();

        if (this.autosearch) {
            $result.on("keypress", function (e) {
                if (e.which === 13) {
                    grid.search();
                    e.preventDefault();
                }
            });
        }

        return $result;
    },

    insertTemplate: function () {
        if (!this.inserting)
            return "";

        var $result = this.insertControl = this._createTextBox();
        return $result;
    },

    editTemplate: function (value) {
        if (!this.editing)
            return this.itemTemplate(value);

        var $result = this.editControl = this._createTextBox();
        $result.val(value);
        return $result;
    },

    filterValue: function () {
        return this.filterControl.val();
    },

    insertValue: function () {
        return this.insertControl.val();
    },

    editValue: function () {
        return this.editControl.val();
    },

    _createTextBox: function () {
        var $result = $("<input>").attr("type", "text").attr("readonly", "readonly");

        if (this.className !== undefined && this.className !== null) {
            $result.addClass(this.className);
        }

        return $result;
    }
});

SelectWithClassField.prototype = new jsGrid.Field({

    align: "center",
    valueType: "text",

    itemTemplate: function (value) {
        var items = this.items,
            valueField = this.valueField,
            textField = this.textField,
            resultItem;

        if (valueField) {
            resultItem = $.grep(items, function (item, index) {
                return item[valueField] === value;
            })[0] || {};
        }
        else {
            resultItem = items[value];
        }

        var result = (textField ? resultItem[textField] : resultItem);

        return (result === undefined || result === null) ? "" : result;
    },

    filterTemplate: function () {
        if (!this.filtering)
            return "";

        var grid = this._grid,
            $result = this.filterControl = this._createSelect();

        if (this.autosearch) {
            $result.on("change", function (e) {
                grid.search();
            });
        }

        return $result;
    },

    insertTemplate: function () {
        if (!this.inserting)
            return "";

        var $result = this.insertControl = this._createSelect();
        return $result;
    },

    editTemplate: function (value) {
        if (!this.editing)
            return this.itemTemplate(value);

        var $result = this.editControl = this._createSelect();
        (value !== undefined) && $result.val(value);
        return $result;
    },

    filterValue: function () {
        var val = this.filterControl.val();
        return this.valueType === "number" ? parseInt(val || 0, 10) : val;
    },

    insertValue: function () {
        var val = this.insertControl.val();
        return this.valueType === "number" ? parseInt(val || 0, 10) : val;
    },

    editValue: function () {
        var val = this.editControl.val();
        return this.valueType === "number" ? parseInt(val || 0, 10) : val;
    },

    _createSelect: function () {
        var $result = $("<select>"),
            valueField = this.valueField,
            textField = this.textField,
            selectedIndex = this.selectedIndex;

        $.each(this.items, function (index, item) {
            var value = valueField ? item[valueField] : index,
                text = textField ? item[textField] : item;

            var $option = $("<option>")
                .attr("value", value)
                .text(text)
                .appendTo($result);

            $option.prop("selected", (selectedIndex === index));
        });

        if (this.className !== undefined && this.className !== null) {
            $result.addClass(this.className);
        }

        return $result;
    }
});

NumberWithClassField.prototype = new jsGrid.TextField({

    sorter: "number",
    align: "right",

    filterValue: function () {
        return parseInt(this.filterControl.val() || 0, 10);
    },

    insertValue: function () {
        return parseInt(this.insertControl.val() || 0, 10);
    },

    editValue: function () {
        return parseInt(this.editControl.val() || 0, 10);
    },

    _createTextBox: function () {
        var $result = $("<input>").attr("type", "number");

        if (this.className !== undefined && this.className !== null) {
            $result.addClass(this.className);
        }

        return $result;
    }
});


/* TextWithClassField.prototype = new jsGrid.Field({

    autosearch: true,

    filterTemplate: function () {
        if (!this.filtering)
            return "";

        var grid = this._grid,
            $result = this.filterControl = this._createTextBox();

        if (this.autosearch) {
            $result.on("keypress", function (e) {
                if (e.which === 13) {
                    grid.search();
                    e.preventDefault();
                }
            });
        }

        return $result;
    },

    insertTemplate: function () {
        if (!this.inserting)
            return "";

        var $result = this.insertControl = this._createTextBox();
        return $result;
    },

    editTemplate: function (value) {
        if (!this.editing)
            return this.itemTemplate(value);

        var $result = this.editControl = this._createTextBox();
        $result.val(value);
        return $result;
    },

    filterValue: function () {
        return this.filterControl.val();
    },

    insertValue: function () {
        return this.insertControl.val();
    },

    editValue: function () {
        return this.editControl.val();
    },

    _createTextBox: function () {
        return $("<input>").attr("type", "text");
    }
}); */

jsGrid.fields.date = MyDateField;
jsGrid.fields.time = MyTimeField;
jsGrid.fields.readonly = ReadOnlyField;
jsGrid.fields.selectwithclass = SelectWithClassField;
// jsGrid.fields.textwithclass = TextWithClassField;
jsGrid.fields.numberwithclass = NumberWithClassField;

var MyCustomDirectLoadStrategy = function (grid) {
    jsGrid.loadStrategies.DirectLoadingStrategy.call(this, grid);
};

MyCustomDirectLoadStrategy.prototype = new jsGrid.loadStrategies.DirectLoadingStrategy();

MyCustomDirectLoadStrategy.prototype.loadParams = function () {
    return {
        WorkshopLineId: $("#WorkshopLineId").val(),
        Date: $("#ProductionDate").val()
    };
};